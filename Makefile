# Start dev environment
up:
	docker-compose -f .docker/docker-compose.yml up -d --remove-orphans;
	@echo 'App is running on http://localhost:8081';

# Start dev environment with forced build
up\:build:
	docker-compose -f .docker/docker-compose.yml up -d --build;
	@echo 'App is running on http://localhost:8081';

# Stop dev environment
down:
	docker-compose -f .docker/docker-compose.yml down;

# Show logs - format it using less
logs:
	docker-compose -f .docker/docker-compose.yml logs -f --tail=10 | less -S +F;

# Exec sh on php container
exec\:php:
	docker-compose -f .docker/docker-compose.yml exec php sh;

# Mazání temp/cache
clean:
	docker-compose -f .docker/docker-compose.yml exec php rm -rf temp/cache

cs:
	docker-compose -f .docker/docker-compose.yml exec php ./vendor/bin/phpcs --cache=./qa/phpcs/phpcs.cache --standard=./qa/phpcs/ruleset.xml --extensions=php --encoding=utf-8 --tab-width=4 -sp --colors app

cs-fix:
	docker-compose -f .docker/docker-compose.yml exec php ./vendor/bin/phpcbf --standard=./qa/phpcs/ruleset.xml --extensions=php --encoding=utf-8 --tab-width=4 --colors app

phpstan:
	docker-compose -f .docker/docker-compose.yml exec php ./vendor/bin/phpstan analyse -c ./qa/phpstan/phpstan.neon --ansi

phpstan-baseline:
	docker-compose -f .docker/docker-compose.yml exec php php -d memory_limit=1G vendor/bin/phpstan analyse -c qa/phpstan/phpstan.neon --generate-baseline