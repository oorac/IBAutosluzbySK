<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'dcff8de8a4842b6f3b2efa043a74da0ac94afc2d',
        'name' => 'nette/web-project',
        'dev' => true,
    ),
    'versions' => array(
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.14.0',
            'version' => '4.14.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'reference' => '12ab42bd6e742c70c0a52f7b82477fcd44e64b75',
            'dev_requirement' => false,
        ),
        'h4kuna/assets' => array(
            'pretty_version' => 'v1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../h4kuna/assets',
            'aliases' => array(),
            'reference' => 'd204d815e0d81d5dc6b31df437a71fe45b85ba2d',
            'dev_requirement' => false,
        ),
        'latte/latte' => array(
            'pretty_version' => 'v2.11.1',
            'version' => '2.11.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../latte/latte',
            'aliases' => array(),
            'reference' => '21316b42fb0a4e43df01f73c55d52bd531823fda',
            'dev_requirement' => false,
        ),
        'nette/application' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/application',
            'aliases' => array(),
            'reference' => 'fa5da6a90ff71724353568894a4839aec627eae3',
            'dev_requirement' => false,
        ),
        'nette/bootstrap' => array(
            'pretty_version' => 'v3.1.2',
            'version' => '3.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/bootstrap',
            'aliases' => array(),
            'reference' => '3ab4912a08af0c16d541c3709935c3478b5ee090',
            'dev_requirement' => false,
        ),
        'nette/caching' => array(
            'pretty_version' => 'v3.1.2',
            'version' => '3.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/caching',
            'aliases' => array(),
            'reference' => '27d8f0048eb1a9c7e49e0268f39b2db7d3ce7ae9',
            'dev_requirement' => false,
        ),
        'nette/component-model' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/component-model',
            'aliases' => array(),
            'reference' => '20a39df12009029c7e425bc5e0439ee4ab5304af',
            'dev_requirement' => false,
        ),
        'nette/database' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/database',
            'aliases' => array(),
            'reference' => 'b138afb94d6ce93c3a7ad9786c2e925ac1ac501f',
            'dev_requirement' => false,
        ),
        'nette/di' => array(
            'pretty_version' => 'v3.0.13',
            'version' => '3.0.13.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/di',
            'aliases' => array(),
            'reference' => '9878f2958a0a804b08430dbc719a52e493022739',
            'dev_requirement' => false,
        ),
        'nette/finder' => array(
            'pretty_version' => 'v2.5.3',
            'version' => '2.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/finder',
            'aliases' => array(),
            'reference' => '64dc25b7929b731e72a1bc84a9e57727f5d5d3e8',
            'dev_requirement' => false,
        ),
        'nette/forms' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/forms',
            'aliases' => array(),
            'reference' => '4ed52434b61d7e532cb3bc77b048717703b91b0b',
            'dev_requirement' => false,
        ),
        'nette/http' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/http',
            'aliases' => array(),
            'reference' => '65bfe68f9c611e7cd1935a5f794a560c52e4614f',
            'dev_requirement' => false,
        ),
        'nette/mail' => array(
            'pretty_version' => 'v3.1.8',
            'version' => '3.1.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/mail',
            'aliases' => array(),
            'reference' => '69b43ae9a5c63ff68804531ef0113c372c676ce6',
            'dev_requirement' => false,
        ),
        'nette/neon' => array(
            'pretty_version' => 'v3.3.3',
            'version' => '3.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/neon',
            'aliases' => array(),
            'reference' => '22e384da162fab42961d48eb06c06d3ad0c11b95',
            'dev_requirement' => false,
        ),
        'nette/php-generator' => array(
            'pretty_version' => 'v3.6.7',
            'version' => '3.6.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/php-generator',
            'aliases' => array(),
            'reference' => 'b9ba414c9895fd9420887f20eeb4eabde123677f',
            'dev_requirement' => false,
        ),
        'nette/robot-loader' => array(
            'pretty_version' => 'v3.4.1',
            'version' => '3.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/robot-loader',
            'aliases' => array(),
            'reference' => 'e2adc334cb958164c050f485d99c44c430f51fe2',
            'dev_requirement' => false,
        ),
        'nette/routing' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/routing',
            'aliases' => array(),
            'reference' => '5532e7e3612e13def357f089c1a5c25793a16843',
            'dev_requirement' => false,
        ),
        'nette/safe' => array(
            'pretty_version' => 'v0.9',
            'version' => '0.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/safe',
            'aliases' => array(),
            'reference' => '8ebf13bdcd5e0ae5394671afa3a6deae70dc01cf',
            'dev_requirement' => false,
        ),
        'nette/schema' => array(
            'pretty_version' => 'v1.2.2',
            'version' => '1.2.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/schema',
            'aliases' => array(),
            'reference' => '9a39cef03a5b34c7de64f551538cbba05c2be5df',
            'dev_requirement' => false,
        ),
        'nette/security' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/security',
            'aliases' => array(),
            'reference' => 'c120893f561b09494486c66594720b2abcb099b2',
            'dev_requirement' => false,
        ),
        'nette/tester' => array(
            'pretty_version' => 'v2.4.2',
            'version' => '2.4.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/tester',
            'aliases' => array(),
            'reference' => '2e788e243bb17a6889aac5411f3fabc48cc5b23a',
            'dev_requirement' => true,
        ),
        'nette/utils' => array(
            'pretty_version' => 'v3.2.7',
            'version' => '3.2.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/utils',
            'aliases' => array(),
            'reference' => '0af4e3de4df9f1543534beab255ccf459e7a2c99',
            'dev_requirement' => false,
        ),
        'nette/web-project' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'dcff8de8a4842b6f3b2efa043a74da0ac94afc2d',
            'dev_requirement' => false,
        ),
        'phpstan/extension-installer' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../phpstan/extension-installer',
            'aliases' => array(),
            'reference' => '66c7adc9dfa38b6b5838a9fb728b68a7d8348051',
            'dev_requirement' => true,
        ),
        'phpstan/phpstan' => array(
            'pretty_version' => '1.6.8',
            'version' => '1.6.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpstan/phpstan',
            'aliases' => array(),
            'reference' => 'd76498c5531232cb8386ceb6004f7e013138d3ba',
            'dev_requirement' => true,
        ),
        'phpstan/phpstan-nette' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'phpstan-extension',
            'install_path' => __DIR__ . '/../phpstan/phpstan-nette',
            'aliases' => array(),
            'reference' => 'f4654b27b107241e052755ec187a0b1964541ba6',
            'dev_requirement' => true,
        ),
        'symfony/thanks' => array(
            'pretty_version' => 'v1.2.10',
            'version' => '1.2.10.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/thanks',
            'aliases' => array(),
            'reference' => 'e9c4709560296acbd4fe9e12b8d57a925aa7eae8',
            'dev_requirement' => true,
        ),
        'tracy/tracy' => array(
            'pretty_version' => 'v2.9.1',
            'version' => '2.9.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tracy/tracy',
            'aliases' => array(),
            'reference' => '4180b3221ff852fe10d5eab30d80be6f6ab7221e',
            'dev_requirement' => false,
        ),
    ),
);
