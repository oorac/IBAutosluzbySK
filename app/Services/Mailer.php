<?php

namespace App\Services;

use App\Model\Settings;
use Latte\Engine;
use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;
use Nette\Neon\Exception;
use Tracy\Debugger;

class Mailer
{
	private Settings $settings;
	private Root $root;

	public function __construct(Settings $settings, Root $root)
	{
		$this->settings = $settings;
		$this->root = $root;
	}

	public function sendMail($emailTo, $emailFrom, $message = '', $subject = '', $htmlBody = []):bool
	{
		try{
			$mail = new Message();
			$mail->setFrom($emailFrom)
				->addTo($emailTo);
			if(empty($htmlBody))
			{
				$mail->setSubject($subject)
					->setBody($message);
			}
			else
			{
				$latte = new Engine;
				$web = $this->settings->getPageInfoAllPairs();
				$params = array(
					'namePage' => $web['title'],
					'streetPage' => $web['street'],
					'cityPage' => $web['city'],
					'zipcodePage' => $web['zipcode'],
					'statePage' => $web['state'],
					'basePath' => $this->root->getBasePath(),
				);
				foreach ($htmlBody['params'] as $key => $param)
				{
					$params[$key] = $param;
				}
				$mail->setHtmlBody($latte->renderToString($htmlBody['templatesPath'] . '/Services/templates/' . $htmlBody['latte'], $params), $htmlBody['imagePath']);
			}

			$mailer = new SmtpMailer([
				'host' => 'wes1-smtp.wedos.net',
				'username' => 'info@ibautosluzby.sk',
				'password' => '112114Alexandra?',
				'secure' => 'tls',
				'port' => '587',
			]);
			$mailer->send($mail);
			return true;
		}
		catch(Exception $e)
		{
			Debugger::log($e, Debugger::ERROR);
			return false;
		}
	}
}