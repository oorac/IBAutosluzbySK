<?php

namespace App\Services;

class Root
{
	public $appDir;
	public $wwwDir;

	public function __construct($appDir, $wwwDir)
	{
		$this->appDir = $appDir;
		$this->wwwDir = $wwwDir;
	}

	public function getBasePath()
	{
		return $this->appDir;
	}

	public function getWwwPath()
	{
		return $this->wwwDir;
	}
}