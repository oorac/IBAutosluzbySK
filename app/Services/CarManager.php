<?php

namespace App\Services;

use App\Model\Car;
use App\Model\Settings;

class CarManager
{

	private Car $car;
	private Settings $settings;
	private Mailer $mailer;

	public function __construct(Car $car, Settings $settings, Mailer $mailer)
	{
		$this->car = $car;
		$this->settings = $settings;
		$this->mailer = $mailer;
	}

	public function deleteImageById($carId, $imageId)
	{
		$this->car->deleteImage($imageId);
		$this->car->deleteCarImage($carId, $imageId);
	}

	public function deleteCar($carId)
	{
		$this->car->database->beginTransaction();
		try
		{
			$carImages = $this->car->getAllCarImagesByCarId($carId);
			$images = $this->car->getAllImagesByCarId($carId);
			if($carImages)
				foreach ($carImages as $carImage) {
					$carImage->delete();
				}
			if($images)
				foreach ($images as $image) {
					$image->delete();
				}
			$parameters = $this->car->getAllParametersByCarId($carId)->fetchAll();
			if($parameters)
				foreach ($parameters as $parameter) {
					$parameter->delete();
				}
			$this->car->deleteCar($carId);
		}
		catch(\Exception $e)
		{
			$web = $this->settings->getPageInfoAllPairs();
			$this->mailer->sendMail('info@idop.cz', $web['email'], $e, 'Problém při mazání auta na IBAutoslužby.sk');
			$this->car->database->rollBack();
			return false;
		}
		$this->car->database->commit();
		return true;
	}

	public function deleteParameter($parameterId)
	{
		$this->car->database->beginTransaction();
		try
		{
			$carParameters = $this->car->getAllCarByParameterId($parameterId);
			if($carParameters)
				return false;
			$this->car->deleteParameter($parameterId);
		}
		catch(\Exception $e)
		{
			$web = $this->settings->getPageInfoAllPairs();
			$this->mailer->sendMail('info@idop.cz', $web['email'], $e, 'Problém při mazání parametru na IBAutoslužby.sk');
			$this->car->database->rollBack();
			return false;
		}
		$this->car->database->commit();
		return true;
	}

	public function hiddenCar($carId)
	{
		$this->car->hiddenCar($carId);
	}

	public function visibleCar($carId)
	{
		$this->car->visibleCar($carId);
	}

	public function hiddenParameter($parameterId)
	{
		$this->car->hiddenParameter($parameterId);
	}

	public function visibleParameter($parameterId)
	{
		$this->car->visibleParameter($parameterId);
	}

	public function getTransmissionCount()
	{
		$transmissions = $this->car->getAllTransmissions()->where('NOT hidden')->fetchAssoc('id');
		$transmissionCount = [];
		foreach ($transmissions as $transmission)
		{
			$count = $this->car->getCountCarByTransmissionId($transmission['id']);
			$transmission['count'] = $count['count'];
			$transmissionCount[$transmission['id']] = $transmission;
		}
		return $transmissionCount;
	}

	public function getParameterCount()
	{
		$parameters = $this->car->getAllParameters()->where('NOT hidden')->fetchAssoc('id');
		$parameterCount = [];
		foreach ($parameters as $parameter)
		{
			$count = $this->car->getCountCarByParameterId($parameter['id']);
			$parameter['count'] = $count['count'];
			$parameterCount[$parameter['id']] = $parameter;
		}
		return $parameterCount;
	}

	public function getAllParametersByGroupParameters()
	{
		$groups = $this->car->getAllGroupParameters()->fetchAssoc('id');
		$groupsCount = [];
		foreach ($groups as $group)
		{
			$parameters = $this->car->getAllParametersByParameterGroupId($group['id'])->fetchAssoc('id');
			$groupsCount[$group['id']] = array(
				'parameters' => $parameters,
				'name' => $group['name'],
				'id' => $group['id'],
			);
		}
		return $groupsCount;
	}

	public function getVolumeCount()
	{
		$volumes = $this->car->getAllVolume()->where('NOT hidden')->fetchAssoc('id');
		$volumeCount = [];
		foreach ($volumes as $volume)
		{
			$count = $this->car->getCountCarByTransmissionId($volume['id']);
			$volume['count'] = $count['count'];
			$volumeCount[$volume['id']] = $volume;
		}
		return $volumeCount;
	}

	public function getPerformanceCount()
	{
		$performances = $this->car->getAllPerformance()->where('NOT hidden')->fetchAssoc('id');
		$performanceCount = [];
		foreach ($performances as $performance)
		{
			$count = $this->car->getCountCarByPerformanceId($performance['id']);
			$performance['count'] = $count['count'];
			$performanceCount[$performance['id']] = $performance;
		}
		return $performanceCount;
	}

	public function getBodyworkCount()
	{
		$bodyworks = $this->car->getAllBodywork()->where('NOT hidden')->fetchAssoc('id');
		$bodyworkCount = [];
		foreach ($bodyworks as $bodywork)
		{
			$count = $this->car->getCountCarByBodyworkId($bodywork['id']);
			$bodywork['count'] = $count['count'];
			$bodyworkCount[$bodywork['id']] = $bodywork;
		}
		return $bodyworkCount;
	}

	public function getFuelCount()
	{
		$fuels = $this->car->getAllFuels()->where('NOT hidden')->fetchAssoc('id');
		$fuelCount = [];
		foreach ($fuels as $fuel)
		{
			$count = $this->car->getCountCarByFuelId($fuel['id']);
			$fuel['count'] = $count['count'];
			$fuelCount[$fuel['id']] = $fuel;
		}
		return $fuelCount;
	}

	public function getCarsCountBySpeedometer($speedometers)
	{
		$speedometerCount = [];
		foreach ($speedometers as $speedometer => $key)
		{
			$count = $this->car->getCountCarBySpeedometer($speedometer);
			$speedometerCount[$speedometer] = array(
				'id' => $speedometer,
				'value' => $key,
				'count' => $count,
			);
		}
		return $speedometerCount;
	}

	public function getCarsCountByVariable($array, $name)
	{
		$recordCount = [];
		foreach ($array as $item => $key)
		{
			$count = $this->car->getCountCarsByToVariable($name, $item);
			bdump($count);
			$recordCount[$item]['count'] = $count['count'];
		}

		return $recordCount;
	}

}