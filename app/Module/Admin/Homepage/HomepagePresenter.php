<?php declare(strict_types=1);

namespace App\Module\Admin\Homepage;

use App\Module\Admin\BasePresenter;
use Nette\Http\IResponse;

final class HomepagePresenter extends BasePresenter
{
    public function actionDefault()
    {
        $this->template->lastCars = $this->car->getAllInfoAboutCarsAll(10);
        $this->template->totalCars = $this->car->getAllInfoAboutCarsCount();
//        $this->template->totalRequests = '';
        $this->template->totalReferencies = $this->reference->getAllReferenceVisibleCount();
        $this->template->totalHiddenCar = $this->car->getAllInfoAboutCarsCount(null, true);
    }

	public function handleDeleteCar($carId)
	{
		$response = $this->carManager->deleteCar($carId);
		if($response)
		{
			$this->flashMessage('Auto bylo odstraněno', self::FM_SUCCESS);
			$this->redirect(':Admin:Homepage:');
		}
		else
		{
			$this->flashMessage('Nastala chyba pri mazání áuta. Správa bola odoslaná programátorovi.', self::FM_ERROR);
			$this->redirect(':Admin:Homepage:');
		}
	}

	public function handleHiddenCar($carId)
	{
		$this->car->hiddenCar($carId);
		$this->flashMessage('Auto bylo skryto', self::FM_INFO);
		$this->redirect(':Admin:Homepage:');
	}

	public function handleVisibleCar($carId)
	{
		$this->car->visibleCar($carId);
		$this->flashMessage('Auto bylo zveřejněno', self::FM_INFO);
		$this->redirect(':Admin:Homepage:');
	}
}