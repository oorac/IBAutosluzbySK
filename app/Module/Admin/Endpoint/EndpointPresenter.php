<?php

namespace App\Module\Admin\Endpoint;

use App\Module\Admin\BasePresenter;

class EndpointPresenter extends BasePresenter
{
	public function actionMark($id): void
	{
		$items = $this->car->getAllMarksByTypeIdPairs($id);
		$this->sendJson($items);
	}

	public function actionModel($id): void
	{
		$items = $this->car->getAllModelByMarkIdPairs($id);
		$this->sendJson($items);
	}

	public function actionEngine($id): void
	{
		$items = $this->car->getAllEngineByModelIdPairs($id);
		$this->sendJson($items);
	}
}