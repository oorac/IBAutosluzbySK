<?php declare(strict_types=1);

namespace App\Module\Admin\Settings\Block;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class BlockPresenter extends BasePresenter
{
    public const LIST_PAGES = [
      'base' => 'Predvolené',
      'homepage' => 'Úvodná',
      'gdpr' => 'GDPR',
      'vop' => 'VOP',
      'service' => 'Služby',
      'add_service' => 'Doplnkové služby',
      'car' => 'Autá',
      'about' => 'O nás',
    ];

    public function actionAdd(): void
    {
        $form = $this->getComponent('blockCreateEdit');
        $form->onSuccess[] = [$this, 'createBlock'];
    }

    public function actionEdit($blockId): void
    {
        $block = $this->block->getContentById($blockId);
        if (!$block) {
            $this->error();
        }

        $form = $this->getComponent('blockCreateEdit');
        $form->setDefaults($block);
        $form->onSuccess[] = [$this, 'editBlock'];
    }

    public function handleDelete($blockId)
    {
        $this->template->block = $this->block->getAllInfoAboutCarById($blockId);
    }

    protected function createComponentBlockCreateEdit(): Form
    {
        if (!in_array($this->getAction(), ['add', 'edit'])) {
            $this->error();
        }
        $form = new Form;
        $form->addHidden('id');
        $form->addSelect('page', 'Pre stránku', self::LIST_PAGES)
            ->setRequired('"Pre stránku" je povinný');
        $form->addText('name', 'Názov bloku')
            ->setRequired('"Názov bloku" je povinný');
        $form->addTextArea('text', 'Obsah bloku')
            ->setHtmlId('editor');
        $form->addCheckbox('default', 'Výchozí')
			->setDisabled();
        $form->addSubmit('submit', 'Uložit');
        return $form;
    }

    public function createBlock(Form $form, array $data): void
    {
		if(empty($data['text']))
		{
			$this->flashMessage('Obsah bloku je povinný', self::FM_WARNING);
			return;
		}
		if(empty($data['name']))
		{
			$this->flashMessage('Názov bloku je povinný', self::FM_WARNING);
			return;
		}
        unset($data['id']);
        $data['default'] = 0;
		try
		{
			$this->block->add($data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Block sa nepodarilo pridať',self::FM_ERROR);
			$this->redirect(':Admin:Settings:block');
		}

        $this->flashMessage('Úspešne pridané', self::FM_SUCCESS);
        $this->redirect(':Admin:Settings:block');
    }

    public function editBlock(Form $form, array $data): void
    {
		if(empty($data['text']))
		{
			$this->flashMessage('Obsah bloku je povinný', self::FM_WARNING);
			return;
		}
		if(empty($data['name']))
		{
			$this->flashMessage('Názov bloku je povinný', self::FM_WARNING);
			return;
		}
        $id = $data['id'];
        unset($data['id']);
		try
		{
			$this->block->update($id, $data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Úprava sa nezdarila',self::FM_ERROR);
			$this->redirect(':Admin:Settings:block');
		}
        $this->flashMessage('Úspešne upravené',self::FM_SUCCESS);
        $this->redirect(':Admin:Settings:block');
    }
}