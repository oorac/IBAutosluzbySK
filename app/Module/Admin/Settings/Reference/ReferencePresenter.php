<?php declare(strict_types=1);

namespace App\Module\Admin\Settings\Reference;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class ReferencePresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('referenceCreateEdit');
		$form->onSuccess[] = [$this, 'createReference'];
	}

	public function actionEdit($referenceId): void
	{
		$reference = $this->settings->getReferenceById($referenceId);
		if (!$reference) {
			$this->error();
		}

		$form = $this->getComponent('referenceCreateEdit');
		$form->setDefaults($reference);
		$form->onSuccess[] = [$this, 'editReference'];
	}

	protected function createComponentReferenceCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addText('name', 'Meno');
		$form->addTextArea('text', 'Obsah referencie')
			->addRule($form::LENGTH, 'Zadajte minimálne %1$d a maximálne %2$d znakov.', [5, 255]);
		$form->onSuccess[] = [$this, 'processForm'];
		$form->addSubmit('submit', 'Uložit');
		return $form;
	}

	public function createReference(Form $form, array $data): void
	{
		if(empty($data['text']))
		{
			$this->flashMessage('Obsah referencie je povinný', self::FM_WARNING);
			return;
		}
		unset($data['id']);
		try
		{
			$this->reference->addReference($data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Referenciu sa nepodarilo pridať',self::FM_ERROR);
			$this->redirect(':Admin:Settings:reference');
		}

		$this->flashMessage('Úspešne pridané', self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:reference');
	}

	public function editReference(Form $form, array $data): void
	{
		if(empty($data['text']))
		{
			$this->flashMessage('Obsah referencie je povinný', self::FM_WARNING);
			return;
		}
		$id = $data['id'];
		unset($data['id']);
		try
		{
			$this->reference->updateReference($id, $data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Úprava sa nezdarila',self::FM_ERROR);
			$this->redirect(':Admin:Settings:reference');
		}
		$this->flashMessage('Úspešne upravené',self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:reference');
	}
}