<?php declare(strict_types=1);

namespace App\Module\Admin\Settings;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class SettingsPresenter extends BasePresenter
{
	public function actionMain()
	{
		$this->template->settings = $this->settings->getPageInfoAll();
	}

	protected function createComponentEditPageInfoForm(): Form
	{
		$form = new Form();
		$settings = $this->settings->getPageInfoAll();
		foreach ($settings as $setting)
		{
			$container = $form->addContainer($setting['id']);
			$container->addText('value', 'Hodnota');
		}
		$form->addSubmit('sent', 'Uložit');
		$form->onRender[] = [$this, 'fillFormPageInfo'];
		$form->onSuccess[] = [$this, 'updatePageInfo'];
		return $form;
	}

	public function updatePageInfo(Form $form, $values)
	{
		$settings = json_decode(json_encode($values), true);
		$keysOfSettings = array_keys($settings);
		foreach ($keysOfSettings as $keysOfSetting)
		{
			$this->settings->updatePageInfo($settings[$keysOfSetting], $keysOfSetting);
		}
	}

	public function fillFormPageInfo(Form $form)
	{
		$settings = $this->settings->getPageInfoAll();
		$defaults = [];
		foreach ($settings as $setting) {
			$setting = $this->settings->getPageInfoById($setting['id']);
			$defaults[$setting['id']]['value'] = $setting['value'];
		}
		$form->setDefaults($defaults);
	}

	public function actionReference()
	{
		$this->template->referencies = $this->reference->getAllReference();
	}

	public function handleDeleteReference($refId)
	{
		$this->reference->deleteReference($refId);
		$this->flashMessage('Úspešne zmazané', self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:reference');
	}

	public function handleHiddenReference($refId)
	{
		$this->reference->hiddenReference($refId);
		$this->flashMessage('Reference bola skryta', self::FM_INFO);
		$this->redirect(':Admin:Settings:reference');
	}

	public function handleVisibleReference($vatId)
	{
		$this->reference->visibleReference($vatId);
		$this->flashMessage('Reference bola zveřejněa', self::FM_INFO);
		$this->redirect(':Admin:Settings:reference');
	}

    public function actionVat()
    {
		$this->template->vats = $this->settings->getAllVat();
    }

	public function handledeleteVat($vatId)
	{
		$this->settings->deleteVat($vatId);
		$this->flashMessage('Úspešne zmazané', self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:vat');
	}

	public function handleHiddenVat($vatId)
	{
		$this->settings->hiddenVat($vatId);
		$this->flashMessage('Sadzba DPH bola skryta', self::FM_INFO);
		$this->redirect(':Admin:Settings:vat');
	}

	public function handleVisibleVat($vatId)
	{
		$this->settings->visibleVat($vatId);
		$this->flashMessage('Sadzba DPH bola zveřejněa', self::FM_INFO);
		$this->redirect(':Admin:Settings:vat');
	}

    public function actionLog()
    {

    }

    public function actionBlock()
    {
        $this->template->blocks = $this->block->getAllBlocks();
    }

    public function handleDeleteBlock($blockId)
    {
        $this->block->delete($blockId);
        $this->flashMessage('Úspešne zmazané', self::FM_SUCCESS);
        $this->redirect(':Admin:Settings:block');
    }
}