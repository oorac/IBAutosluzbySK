<?php declare(strict_types=1);

namespace App\Module\Admin\Settings\Vat;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class VatPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('vatCreateEdit');
		$form->onSuccess[] = [$this, 'createVat'];
	}

	public function actionEdit($vatId): void
	{
		$vat = $this->settings->getVatById($vatId);
		if (!$vat) {
			$this->error();
		}

		$form = $this->getComponent('vatCreateEdit');
		$form->setDefaults($vat);
		$form->onSuccess[] = [$this, 'editVat'];
	}

	protected function createComponentVatCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov sadzby');
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createVat(Form $form, array $data): void
	{
		if($data['name'] == '' || is_null($data['name']))
		{
			$this->flashMessage('Názov sadzby je povinný', self::FM_WARNING);
			return;
		}
		unset($data['id']);
		try
		{
			$this->settings->addVat($data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Sadzbu DPH sa nepodarilo pridať',self::FM_ERROR);
			$this->redirect(':Admin:Settings:vat');
		}

		$this->flashMessage('Úspešne pridané', self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:vat');
	}

	public function editVat(Form $form, array $data): void
	{
		if(empty($data['name']))
		{
			$this->flashMessage('Názov sadzby je povinný', self::FM_WARNING);
			return;
		}
		$id = $data['id'];
		unset($data['id']);
		try
		{
			$this->settings->updateVat($id, $data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Úprava sa nezdarila',self::FM_ERROR);
			$this->redirect(':Admin:Settings:vat');
		}

		$this->flashMessage('Úspešne upravené',self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:vat');
	}
}