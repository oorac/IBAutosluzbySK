<?php declare(strict_types=1);

namespace App\Module\Admin\Settings\Price;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class PricePresenter extends BasePresenter
{
	protected function createComponentVatCreateEdit(): Form
	{
		$form = new Form;
		$form->addHidden('id');
		$form->addText('number', 'Čiastka');
		$form->addSubmit('submit', 'Uložiť');
		$form->onSuccess[] = [$this, 'createPrice'];
		return $form;
	}

	public function createPrice(Form $form, array $data): void
	{
		if(empty($data['number']))
		{
			$this->flashMessage('Čiastka je povinná', self::FM_WARNING);
			return;
		}
		unset($data['id']);
		try
		{
			$this->settings->addPrice($data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Čiastku sa nepodarilo pridať',self::FM_ERROR);
			$this->redirect(':Admin:Settings:price');
		}

		$this->flashMessage('Úspešne pridané', self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:price');
	}

	public function editVat(Form $form, array $data): void
	{
		if(empty($data['number']))
		{
			$this->flashMessage('Čiastka je povinná', self::FM_WARNING);
			return;
		}
		$id = $data['id'];
		unset($data['id']);
		try
		{
			$this->settings->updateVat($id, $data);
		}
		catch (\Exception $e)
		{
			$this->flashMessage('Úprava sa nezdarila',self::FM_ERROR);
			$this->redirect(':Admin:Settings:price');
		}

		$this->flashMessage('Úspešne upravené',self::FM_SUCCESS);
		$this->redirect(':Admin:Settings:price');
	}
}