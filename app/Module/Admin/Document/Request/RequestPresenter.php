<?php declare(strict_types=1);

namespace App\Module\Admin\Document\Request;

use App\Module\Admin\BasePresenter;

final class RequestPresenter extends BasePresenter
{
    public function actionDefault()
    {
        $this->template->celkem_aut = '';
        $this->template->celkem_poptavek = '';
    }
}