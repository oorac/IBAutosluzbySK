<?php declare(strict_types=1);

namespace App\Module\Admin\List\Bodywork;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class BodyworkPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('bodyworkCreateEdit');
		$form->onSuccess[] = [$this, 'createBodywork'];
	}

	public function actionEdit($bodyworkId): void
	{
		$bodywork = $this->car->getBodyworkById($bodyworkId);
		if (!$bodywork) {
			$this->error();
		}

		$form = $this->getComponent('bodyworkCreateEdit');
		$form->setDefaults($bodywork);
		$form->onSuccess[] = [$this, 'editBodywork'];
	}

	protected function createComponentBodyworkCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov karosérie');
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createBodywork(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addBodywork($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:bodywork');
	}

	public function editBodywork(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateBodywork($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:bodywork');
	}
}