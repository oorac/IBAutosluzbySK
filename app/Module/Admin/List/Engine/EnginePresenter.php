<?php declare(strict_types=1);

namespace App\Module\Admin\List\Engine;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class EnginePresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('engineCreateEdit');
		$form->onSuccess[] = [$this, 'createEngine'];
	}

	public function actionEdit($engineId): void
	{
		$engine = $this->car->getEngineById($engineId);
		if (!$engine) {
			$this->error();
		}

		$form = $this->getComponent('engineCreateEdit');
		$form->setDefaults($engine);
		$form->onSuccess[] = [$this, 'editEngine'];
	}

	protected function createComponentEngineCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov motora');
		$form->addSelect('model_id', 'Model vozidla', $this->car->getAllTypesPairs())
			->setPrompt('Vyber model vozidla');
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createEngine(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addEngine($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:engine');
	}

	public function editEngine(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateEngine($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:engine');
	}
}