<?php declare(strict_types=1);

namespace App\Module\Admin\List\Fuel;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class FuelPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('fuelCreateEdit');
		$form->onSuccess[] = [$this, 'createFuel'];
	}

	public function actionEdit($fuelId): void
	{
		$fuel = $this->car->getTypeById($fuelId);
		if (!$fuel) {
			$this->error();
		}

		$form = $this->getComponent('fuelCreateEdit');
		$form->setDefaults($fuel);
		$form->onSuccess[] = [$this, 'editFuel'];
	}

	protected function createComponentFuelCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov palivá');
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createFuel(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addFuel($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:fuel');
	}

	public function editType(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateFuel($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:fuel');
	}
}