<?php declare(strict_types=1);

namespace App\Module\Admin\List\Parameter;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class ParameterPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('parameterCreateEdit');
		$form->onSuccess[] = [$this, 'createParameter'];
	}

	public function actionEdit($parameterId): void
	{
		$parameter = $this->car->getParameterId($parameterId);
		if (!$parameter) {
			$this->error();
		}

		$form = $this->getComponent('parameterCreateEdit');
		$form->setDefaults($parameter);
		$form->onSuccess[] = [$this, 'editParameter'];
	}

	protected function createComponentParameterCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'CZ Názov parametru');
		$form->addText('sk_name', 'SK Názov parametru');

		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createParameter(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addParameter($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:parameter');
	}

	public function editParameter(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateParameter($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:parameter');
	}
}