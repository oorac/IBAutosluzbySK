<?php declare(strict_types=1);

namespace App\Module\Admin\List\Type;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class TypePresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('typeCreateEdit');
		$form->onSuccess[] = [$this, 'createType'];
	}

	public function actionEdit($typeId): void
	{
		$type = $this->car->getTypeById($typeId);
		if (!$type) {
			$this->error();
		}

		$form = $this->getComponent('typeCreateEdit');
		$form->setDefaults($type);
		$form->onSuccess[] = [$this, 'editType'];
	}

	protected function createComponentTypeCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov typu');
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createType(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addType($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:type');
	}

	public function editType(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateType($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:type');
	}
}