<?php declare(strict_types=1);

namespace App\Module\Admin\List\Car;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;
use Nette\Utils\Image;

final class CarPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('carCreateEdit');
		$form->onSuccess[] = [$this, 'createCar'];
	}

	public function actionEdit($carId): void
	{
		$car = $this->car->getCarById($carId);
		if (!$car) {
			$this->error();
		}
		$form = $this->getComponent('carCreateEdit');

		$form['mark_id']->setItems($this->car->getAllMarksByTypeIdPairs($car->type_id));
		$form['model_id']->setItems($this->car->getAllModelByMarkIdPairs($car->mark_id));
		$json_condition = json_encode($this->car->getAllParametersByCarId($carId)->fetchAssoc('id'), JSON_PRETTY_PRINT);
		$form->setDefaults([
			'equip_data' => $json_condition
		]);
		$form->setDefaults($car);
		$form->onSuccess[] = [$this, 'editCar'];
		$otherImages = $this->car->getOtherImagesByCarId($carId);
		$mainImage = $this->car->getMainImagesByCarId($carId)->fetchAssoc('id');
		$this->template->images = array_merge($mainImage, $otherImages);
		$this->template->id = $car->id;
	}

	public function handleDeleteImage($carId, $imageId): void
	{
		$this->carManager->deleteImageById($carId, $imageId);
	}

	protected function createComponentCarCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addHidden('equip_data');
		$form->addText('url', 'URL zdroje');
		$form->addTextArea('description', 'Popis auta')
			->setHtmlId('editor');
		$form->addSelect('vat_id', 'Sadzba DPH', $this->car->getAllVatPairs());
		$form->addText('price', 'Cena auta')
			->setDefaultValue(0);
		$form->addText('old_price', 'Pôvodná cena auta')
			->setDefaultValue(0);
		$form->addCheckbox('new', 'Novinka');
		$form->addCheckbox('tip', 'Štítok odporúčané');
		$type = $form->addSelect('type_id', 'Typ vozidla', $this->car->getAllTypesPairs())
			->setPrompt('Vyber typ vozidla');

		// Závislý select mark na type
		$mark = $form->addSelect('mark_id', 'Značka vozidla')
			->setPrompt('Vyber značku vozidla');

		$mark->setHtmlAttribute('data-url', $this->link(':Admin:Endpoint:mark', '#'))
			->setHtmlAttribute('data-depends', $type->getHtmlName());

//		$form->onAnchor[] = fn() =>
//		$mark->setItems($type->getValue() ? $this->car->getAllMarksByTypeIdPairs($type->getValue()) : []);

		$form->onAnchor[] = function () use ($mark, $type){
			$mark->setItems($type->getValue() ? $this->car->getAllMarksByTypeIdPairs($type->getValue()) : []);
		};

		// Další závislý select model na mark
		$model = $form->addSelect('model_id', 'Model vozidla')
			->setPrompt('Vyber model vozidla');

		$model->setHtmlAttribute('data-url', $this->link(':Admin:Endpoint:model', '#'))
			->setHtmlAttribute('data-depends', $mark->getHtmlName());

		$form->onAnchor[] = fn() =>
		$model->setItems($type->getValue() ? $this->car->getAllModelByMarkIdPairs($mark->getValue()) : []);

		$form->addSelect('fuel_id', 'Palivo', $this->car->getAllFuelsPairs())
			->setPrompt('Palivo');
		$form->addSelect('bodywork_id', 'Karoséria', $this->car->getAllBodyworkPairs());
		$form->addSelect('transmission_id', 'Prevodovka', $this->car->getAllTransmissionPairs());
		$form->addText('speedometer', 'Najeto')
			->setDefaultValue(0);

		$actualYear = date('Y');
		$yearArray = [];
		for($i = 2009; $i < $actualYear + 1; $i++)
		{
			$yearArray[$i] = $i;
		}
		$year = array_reverse($yearArray, true);
		$form->addSelect('year', 'Rok výroby', $year)
			->setPrompt('Vyber rok výroby');
		$form->addCheckbox('hidden', 'Zveřejnit');
		$form->addText('performance', 'Výkon', )
				->setDefaultValue(0);
		$form->addText('volume', 'Objem valcov')
			->setDefaultValue(0);
		$form->addSelect('drive_id', 'Pohon', $this->car->getAllDrivePairs())
			->setPrompt('Vyber pohon');
		$form->addUpload('main_image', 'Hlavní obrázek')
			->setHtmlAttribute('accept', 'image/*')
			->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.');
		$form->addMultiUpload('images', 'Ostatné obrázky')
			->setHtmlAttribute('accept', 'image/*')
			->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.');
		$form->addCheckboxList('equip', 'Výbava', $this->car->getParameters());
		$form->addSubmit('submit', 'Uložit');
		return $form;
	}

	public function createCar(Form $form, array $data): void
	{
		$images = $data['images'];
		$main_image = $data['main_image'];
		$equip = $data['equip'];
		unset($data['id']);
		unset($data['equip_data']);
		unset($data['images']);
		unset($data['main_image']);
		unset($data['equip']);
		$this->car->database->beginTransaction();
		try
		{
			$car = $this->car->addCar($data);
			$this->insertImages($main_image, $images, $car->id);
			$this->insertCarParameter($car->id, $equip);
		}
		catch(\Exception $e)
		{
			$web = $this->settings->getPageInfoAllPairs();
			$this->mailer->sendMail('info@idop.cz', $web['email'], $e, 'Problém při ukládání auta na IBAutoslužby.sk');
			$this->car->database->rollBack();
			$this->flashMessage('Nastala chyba pri ukladaní dát. Správa bola odoslaná programátorovi.', self::FM_ERROR);
			$this->redirect(':Admin:List:car');
		}
		$this->car->database->commit();
		$this->flashMessage('Úspešne pridané', self::FM_SUCCESS);
		$this->redirect(':Admin:List:car');
	}

	public function editCar(Form $form, array $data): void
	{
		$id = (int) $data['id'];
		$images = $data['images'];
		$main_image = $data['main_image'];
		$equip = $data['equip'];
		unset($data['id']);
		unset($data['equip_data']);
		unset($data['images']);
		unset($data['main_image']);
		unset($data['equip']);
		$this->car->database->beginTransaction();
		try
		{
			$this->car->updateCar($id, $data);
			$this->insertImages($main_image, $images, $id);
			$this->insertCarParameter($id, $equip);
		}
		catch(\Exception $e)
		{
			$web = $this->settings->getPageInfoAllPairs();
			$this->mailer->sendMail('info@idop.cz', $web['email'], $e, 'Problém při ukládání auta na IBAutoslužby.sk');
			$this->car->database->rollBack();
			$this->flashMessage('Nastala chyba pri ukladaní dát. Správa bola odoslaná programátorovi.', self::FM_ERROR);
			$this->redirect(':Admin:List:car');
		}
		$this->car->database->commit();
		$this->flashMessage('Úspešne upravené',self::FM_SUCCESS);
		$this->redirect(':Admin:List:car');
	}

	private function insertImages($main_image, $images, $id): void
	{
		$image = $this->createImage($main_image, 1);
		if($image !== false)
			$this->car->insertRelationImageCar($image->id, $id);
		foreach ($images as $image)
		{
			$otherImage = $this->createImage($image);
			if($otherImage !== false)
				$this->car->insertRelationImageCar($otherImage->id, $id);
		}
	}

	private function insertCarParameter(int $carId, array $equip = [])
	{
		foreach ($equip as $parameter)
		{
			$this->car->insertCarParameter($carId, $parameter);
		}
	}

	private function createImage($image, $main = 0)
	{
		if ($image->hasFile()) {
			$file_ext = strtolower(mb_substr($image->getSanitizedName(), strrpos($image->getSanitizedName(), ".")));
			$fileNameThumb = strtolower(mb_substr($image->getSanitizedName(), 0, strrpos($image->getSanitizedName(), "."))) . '_thumb' . $file_ext;
			$fileNameFull = $image->getSanitizedName();

			$image->move($this->root->getWwwPath() . $this->car::TEMP_DIR_IMAGE . $fileNameFull);
			$imageToSave = Image::fromFile($this->root->getWwwPath() . $this->car::TEMP_DIR_IMAGE . $fileNameFull);
			$imageToSave->resize(350,null);
			$imageToSave->sharpen();
			$imageToSave->save($this->root->getWwwPath() . $this->car::DIR_IMAGE . $fileNameFull);
			$imageToSave->resize(50,null);
			$imageToSave->sharpen();
			$imageToSave->save($this->root->getWwwPath() . $this->car::DIR_IMAGE . $fileNameThumb);

			$imageSave = array(
				'path' => $fileNameFull,
				'main' => $main,
				'thumb' => $fileNameThumb
			);
			return $this->car->insertImage($imageSave);
		}

		return false;
	}
}