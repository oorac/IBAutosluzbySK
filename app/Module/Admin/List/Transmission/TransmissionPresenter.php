<?php declare(strict_types=1);

namespace App\Module\Admin\List\Transmission;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class TransmissionPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('transmissionCreateEdit');
		$form->onSuccess[] = [$this, 'createTransmission'];
	}

	public function actionEdit($transmissionId): void
	{
		$transmission = $this->car->getTransmissionById($transmissionId);
		if (!$transmission) {
			$this->error();
		}

		$form = $this->getComponent('transmissionCreateEdit');
		$form->setDefaults($transmission);
		$form->onSuccess[] = [$this, 'editTransmission'];
	}

	protected function createComponentTransmissionCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov prevodovky');
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createTransmission(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addTransmission($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:transmission');
	}

	public function editTransmission(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateTransmission($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:transmission');
	}
}