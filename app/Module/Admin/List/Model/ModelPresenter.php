<?php declare(strict_types=1);

namespace App\Module\Admin\List\Model;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class ModelPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('modelCreateEdit');
		$form->onSuccess[] = [$this, 'createModel'];
	}

	public function actionEdit($modelId): void
	{
		$model = $this->car->getModelById($modelId);
		if (!$model) {
			$this->error();
		}

		$form = $this->getComponent('modelCreateEdit');
		$form->setDefaults($model);
		$form->onSuccess[] = [$this, 'editModel'];
	}

	protected function createComponentModelCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov modelu');
		$type = $form->addSelect('type_id', 'Typ vozidla', $this->car->getAllTypesPairs())
			->setPrompt('Vyber typ vozidla');

		// Závislý select mark na type
		$mark = $form->addSelect('mark_id', 'Značka vozidla')
			->setPrompt('Vyber značku vozidla');

		$type->setHtmlAttribute('data-url', $this->link(':Admin:Endpoint:mark', '#'))
			->setHtmlAttribute('data-dependent', $mark->getHtmlName());

		$form->onAnchor[] = fn() =>
		$mark->setItems($type->getValue() ? $this->car->getAllMarksByTypeIdPairs($type->getValue()) : []);

		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createModel(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addModel($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:model');
	}

	public function editModel(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateModel($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:model');
	}
}