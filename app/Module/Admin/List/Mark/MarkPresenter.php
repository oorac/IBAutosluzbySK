<?php declare(strict_types=1);

namespace App\Module\Admin\List\Mark;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class MarkPresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('markCreateEdit');
		$form->onSuccess[] = [$this, 'createMark'];
	}

	public function actionEdit($markId): void
	{
		$model = $this->car->getMarkById($markId);
		if (!$model) {
			$this->error();
		}

		$form = $this->getComponent('markCreateEdit');
		$form->setDefaults($model);
		$form->onSuccess[] = [$this, 'editMark'];
	}

	protected function createComponentMarkCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Názov značky');
		$form->addSelect('type_id', 'Typ vozidla', $this->car->getAllTypesPairs())
			->setPrompt('Vyber typ vozidla');

		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createMark(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addMark($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:mark');
	}

	public function editMark(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateModel($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:mark');
	}
}