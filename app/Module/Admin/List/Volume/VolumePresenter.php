<?php declare(strict_types=1);

namespace App\Module\Admin\List\Volume;

use App\Module\Admin\BasePresenter;
use Nette\Application\UI\Form;

final class VolumePresenter extends BasePresenter
{
	public function actionAdd(): void
	{
		$form = $this->getComponent('volumeCreateEdit');
		$form->onSuccess[] = [$this, 'createVolume'];
	}

	public function actionEdit($volumeId): void
	{
		$volume = $this->car->getVolumeById($volumeId);
		if (!$volume) {
			$this->error();
		}

		$form = $this->getComponent('volumeCreateEdit');
		$form->setDefaults($volume);
		$form->onSuccess[] = [$this, 'editVolume'];
	}

	protected function createComponentVolumeCreateEdit(): Form
	{
		if (!in_array($this->getAction(), ['add', 'edit'])) {
			$this->error();
		}
		$form = new Form;
		$form->addHidden('id');
		$form->addText('name', 'Objem valcov')
			->setRequired();
		$form->addCheckbox('hidden', 'Zverejniť');
		$form->addSubmit('submit', 'Uložiť');
		return $form;
	}

	public function createVolume(Form $form, array $data): void
	{
		unset($data['id']);
		$this->car->addVolumee($data);
		$this->flashMessage('Úspešne pridané', 'success');
		$this->redirect(':Admin:List:volume');
	}

	public function editVolume(Form $form, array $data): void
	{
		$id = $data['id'];
		unset($data['id']);
		$this->car->updateVolume($id, $data);
		$this->flashMessage('Úspešne upravené','success');
		$this->redirect(':Admin:List:volume');
	}
}