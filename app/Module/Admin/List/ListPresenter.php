<?php declare(strict_types=1);

namespace App\Module\Admin\List;

use App\Module\Admin\BasePresenter;
use mysql_xdevapi\Exception;
use Nette\Application\UI\Form;
use Nette\Database\ConstraintViolationException;

final class ListPresenter extends BasePresenter
{
	protected function createComponentEditParameterForm(): Form
	{
		$form = new Form();

		$parameters = $this->car->getParameters();
		foreach ($parameters as $parameter)
		{
			$container = $form->addContainer($parameter->id);
			$container->addSelect('group', 'Skupina', $this->car->getAllGroupParametersPairs())
				->setPrompt('Vyber zo zoznamu');
		}
		$form->addSubmit('sent', 'Uložit');
		$form->onRender[] = [$this, 'fillFormParameter'];
		$form->onSuccess[] = [$this, 'updateParameter'];
		return $form;
	}

	public function updateParameter(Form $form, $values)
	{
		try
		{
			$parameters = json_decode(json_encode($values), true);
			$keysOfParameters = array_keys($parameters);
			foreach ($keysOfParameters as $keysOfParameter)
			{
				$this->car->updateParameter($parameters[$keysOfParameter], $keysOfParameter);
			}
		}
		catch(Exception $e)
		{
			$this->flashMessage('Nepodařilo se uložit '. $e, self::FM_ERROR);
			$this->redirect(':Admin:List:parameter');
		}

		$this->flashMessage('Uloženo', self::FM_SUCCESS);
		$this->redirect(':Admin:List:parameter');
	}

	public function fillFormParameter(Form $form)
	{
		$parameters = $this->car->getParameters();
		$defaults = [];
		foreach ($parameters as $parameter) {
			$parameter = $this->car->getParameterId($parameter['id']);
			$defaults[$parameter['id']]['group'] = $parameter['group'];
		}
		$form->setDefaults($defaults);
	}

	public function actionParameter(): void
	{
		$this->template->parameters = $this->car->getParameters();
	}

	public function handleDeleteParameter($parameterId)
	{
		$response = $this->carManager->deleteParameter($parameterId);
		if($response)
		{
			$this->flashMessage('Auto bylo odstraněno', self::FM_SUCCESS);
			$this->redirect(':Admin:List:parameter');
		}
		else
		{
			$this->flashMessage('Nastala chyba pri mazání áuta. Správa bola odoslaná programátorovi.', self::FM_ERROR);
			$this->redirect(':Admin:List:parameter');
		}
	}

	public function handleHiddenParameter($parameterId)
	{
		$this->carManager->hiddenParameter($parameterId);
		$this->flashMessage('Auto bylo skryto', self::FM_INFO);
		$this->redirect(':Admin:List:parameter');
	}

	public function handleVisibleParameter($parameterId)
	{
		$this->car->visibleParameter($parameterId);
		$this->flashMessage('Auto bylo zveřejněno', self::FM_INFO);
		$this->redirect(':Admin:List:parameter');
	}

    public function actionCar($hidden = false)
    {
		if($hidden)
			$this->template->cars = $this->car->getAllInfoAboutCars(null, null, null, true);
		else
        	$this->template->cars = $this->car->getAllInfoAboutCarsAll();
    }

	public function actionDrive(): void
	{
		$this->template->drives = $this->car->getAllDrive();
	}

	public function handleDeleteDrive($driveId)
	{
		$this->car->deleteDrive($driveId);
		$this->flashMessage('Pohon vozidla byl odstraněn', self::FM_SUCCESS);
		$this->redirect(':Admin:List:drive');
	}

	public function handleHiddenDrive($driveId)
	{
		$this->car->hiddenDrive($driveId);
		$this->flashMessage('Pohon vozidla byl skryt', self::FM_INFO);
		$this->redirect(':Admin:List:drive');
	}

	public function handleVisibleDrive($driveId)
	{
		$this->car->visibleDrive($driveId);
		$this->flashMessage('Pohon vozidla byl zveřejněn', self::FM_INFO);
		$this->redirect(':Admin:List:drive');
	}

    public function handleDeleteCar($carId)
    {
		$response = $this->carManager->deleteCar($carId);
		if($response)
		{
			$this->flashMessage('Auto bylo odstraněno', self::FM_SUCCESS);
			$this->redirect(':Admin:List:car');
		}
		else
		{
			$this->flashMessage('Nastala chyba pri mazání áuta. Správa bola odoslaná programátorovi.', self::FM_ERROR);
			$this->redirect(':Admin:List:car');
		}
    }

    public function handleHiddenCar($carId)
    {
        $this->car->hiddenCar($carId);
        $this->flashMessage('Auto bylo skryto', self::FM_INFO);
		$this->redirect(':Admin:List:car');
    }

    public function handleVisibleCar($carId)
    {
        $this->car->visibleCar($carId);
        $this->flashMessage('Auto bylo zveřejněno', self::FM_INFO);
        $this->redirect(':Admin:List:car');
    }

	public function actionType()
	{
		$this->template->types = $this->car->getAllTypes();
	}

	public function handleDeleteType($typeId)
	{
		try
		{
			$this->car->deleteType($typeId);
		}
		catch (ConstraintViolationException $e)
		{
			$this->car->database->rollBack();
			$this->flashMessage('Nastala chyba pri mazání áuta. Správa bola odoslaná programátorovi.', self::FM_ERROR);
			$this->redirect(':Admin:List:type');
		}
		$this->flashMessage('Typ vozidla byl odstraněn', self::FM_SUCCESS);
		$this->redirect(':Admin:List:type');
	}

	public function handleHiddenType($typeId)
	{
		$this->car->hiddenType($typeId);
		$this->flashMessage('Typ vozidla byl skryt', self::FM_INFO);
		$this->redirect(':Admin:List:type');
	}

	public function handleVisibleType($typeId)
	{
		$this->car->visibleType($typeId);
		$this->flashMessage('Typ vozidla byl zveřejněn', self::FM_INFO);
		$this->redirect(':Admin:List:type');
	}

	public function actionMark()
	{
		$this->template->marks = $this->car->getAllMarks();
	}

	public function handleDeleteMark($markId)
	{
		$this->car->deleteMark($markId);
		$this->flashMessage('Značka vozidla byla odstraněna', self::FM_SUCCESS);
		$this->redirect(':Admin:List:mark');
	}

	public function handleHiddenMark($markId)
	{
		$this->car->hiddenMark($markId);
		$this->flashMessage('Značka vozidla byla skryta', self::FM_INFO);
		$this->redirect(':Admin:List:mark');
	}

	public function handleVisibleMark($markId)
	{
		$this->car->visibleMark($markId);
		$this->flashMessage('Značka vozidla byla zveřejněna', self::FM_INFO);
		$this->redirect(':Admin:List:mark');
	}

	public function actionModel()
	{
		$this->template->models = $this->car->getAllModel();
	}

	public function handleDeleteModel($modelId)
	{
		$this->car->deleteModel($modelId);
		$this->flashMessage('Model vozidla byl odstraněn', self::FM_SUCCESS);
		$this->redirect(':Admin:List:model');
	}

	public function handleHiddenModel($modelId)
	{
		$this->car->hiddenModel($modelId);
		$this->flashMessage('Model vozidla byl skryt', self::FM_INFO);
		$this->redirect(':Admin:List:model');
	}

	public function handleVisibleModel($modelId)
	{
		$this->car->visibleModel($modelId);
		$this->flashMessage('Model vozidla byl zveřejněn', self::FM_INFO);
		$this->redirect(':Admin:List:model');
	}

	public function actionFuel()
	{
		$this->template->fuels = $this->car->getAllFuels()->fetchAssoc('id');
	}

	public function handleDeleteFuel($fuelId)
	{
		$this->car->deleteFuel($fuelId);
		$this->flashMessage('Palivo bylo odstraněno', self::FM_SUCCESS);
		$this->redirect(':Admin:List:fuel');
	}

	public function handleHiddenFuel($fuelId)
	{
		$this->car->hiddenFuel($fuelId);
		$this->flashMessage('Palivo bylo skryto', self::FM_INFO);
		$this->redirect(':Admin:List:fuel');
	}

	public function handleVisibleFuel($fuelId)
	{
		$this->car->visibleFuel($fuelId);
		$this->flashMessage('Palivo bylo zveřejněno', self::FM_INFO);
		$this->redirect(':Admin:List:fuel');
	}

	public function actionTransmission()
	{
		$this->template->transmissions = $this->car->getAllTransmissions()->fetchAll();
	}

	public function handleDeleteTransmission($transmissionId)
	{
		$this->car->deleteTransmission($transmissionId);
		$this->flashMessage('Prevodovka byla odstraněna', self::FM_SUCCESS);
		$this->redirect(':Admin:List:transmission');
	}

	public function handleHiddenTransmission($transmissionId)
	{
		$this->car->hiddenTransmission($transmissionId);
		$this->flashMessage('Prevodovka byla skryta', self::FM_INFO);
		$this->redirect(':Admin:List:transmission');
	}

	public function handleVisibleTransmission($transmissionId)
	{
		$this->car->visibleTransmission($transmissionId);
		$this->flashMessage('Prevodovka byla zveřejněa', self::FM_INFO);
		$this->redirect(':Admin:List:transmission');
	}

	public function actionBodywork()
	{
		$this->template->bodyworks = $this->car->getAllBodywork()->fetchAll();
	}

	public function handleDeleteBodywork($bodyworkId)
	{
		$this->car->deleteBodywork($bodyworkId);
		$this->flashMessage('Karoséria byla odstraněna', self::FM_SUCCESS);
		$this->redirect(':Admin:List:bodywork');
	}

	public function handleHiddenBodywork($bodyworkId)
	{
		$this->car->hiddenBodywork($bodyworkId);
		$this->flashMessage('Karoséria byla skryta', self::FM_INFO);
		$this->redirect(':Admin:List:bodywork');
	}

	public function handleVisibleBodywork($bodyworkId)
	{
		$this->car->visibleBodywork($bodyworkId);
		$this->flashMessage('Karoséria byla zveřejněa', self::FM_INFO);
		$this->redirect(':Admin:List:bodywork');
	}
}