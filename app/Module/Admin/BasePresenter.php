<?php

namespace App\Module\Admin;

use App\Model\Block;
use App\Model\Car;
use App\Model\Reference;
use App\Model\Settings;
use App\Services\CarManager;
use App\Services\Mailer;
use App\Services\Root;
use Nette\Application\UI\Presenter;
use Nette\Http\IResponse;

abstract class BasePresenter extends Presenter
{
    use RequireLoggedUser;
    const FM_SUCCESS = 'success';
    const FM_INFO = 'info';
    const FM_WARNING = 'warning';
    const FM_ERROR = 'danger';

	protected Car $car;
	protected Settings $settings;
	protected Block $block;
	protected Reference $reference;
	protected Root $root;
	protected Mailer $mailer;
	protected CarManager $carManager;

	public function __construct(Root $root, Car $car, Settings $settings, Block $block, Reference $reference, Mailer $mailer, CarManager $carManager)
	{
		parent::__construct();
		$this->car = $car;
		$this->settings = $settings;
		$this->block = $block;
		$this->reference = $reference;
		$this->root = $root;
		$this->mailer = $mailer;
		$this->carManager = $carManager;
	}

    /**
     * Common render method.
     * @return void
     */
    protected function beforeRender()
    {
        parent::beforeRender();
        if (!$this->getUser()->isInRole('ADMIN'))
            $this->error('Nedostatečné oprávnění.', IResponse::S403_FORBIDDEN);
		$this->template->blockBase = $this->block->getContentByName('admin');
        $this->template->basePath = '/';
    }
}
