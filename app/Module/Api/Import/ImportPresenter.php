<?php

namespace App\Module\Api\Import;

use App\Module\Front\BasePresenter;

class ImportPresenter extends BasePresenter
{
	public function actionEquipment(): void
	{
		$dic = $this->root->getBasePath() . '/../www/xml/';
		$readXml = simplexml_load_file($dic . 'vybava.xml');
		$equipments = json_decode(json_encode($readXml), true);
		$response = $this->car->addParameter(array_shift($equipments));

		$this->sendJson($response);
	}

	public function actionCars(): void
	{
		$dic = $this->root->getBasePath() . '/../www/xml/';
		$readXml = simplexml_load_file($dic . 'carList.xml');
		$kinds = json_decode(json_encode($readXml), true);
		foreach ($kinds['kind'] as $kind) {
			$insertType = array(
				'name' => $kind['kind_name']
			);
			$type = $this->car->addType($insertType);
			foreach ($kind['body'] as $body)
			{
				$insertBodywork = array(
					'type_id' => $type->id,
					'name' => $body['body_name'],
				);
				$bodywork = $this->car->addBodywork($insertBodywork);
			}
			foreach ($kind['manufacturer'] as $manufacturer)
			{
				$insertMark =  array(
					'type_id' => $type->id,
					'name' => $manufacturer['manufacturer_name'],
				);
				$mark = $this->car->addMark($insertMark);
				if(isset($manufacturer['model']['model_name']))
				{
					$insertModel =  array(
						'type_id' => $type->id,
						'mark_id' => $mark->id,
						'name' => $manufacturer['model']['model_name'],
					);
					$model = $this->car->addModel($insertModel);
				}
				else
					foreach ($manufacturer['model'] as $modelArray)
					{
						$insertModel =  array(
							'type_id' => $type->id,
							'mark_id' => $mark->id,
							'name' => $modelArray['model_name'],
						);
						$model = $this->car->addModel($insertModel);
					}
			}
		}
		$this->sendJson('hotovo');
	}
}