<?php declare(strict_types=1);

namespace App\Module\Front\Service;

use App\Module\Front\BasePresenter;

final class ServicePresenter extends BasePresenter
{
    public function actionDefault()
    {
        $blocks = $this->block->getContentByName('service');
		$this->template->finance = $this->getHtmlPurifier($blocks['finance']['text']);
		$this->template->service = $this->getHtmlPurifier($blocks['service']['text']);
		$this->template->transfer = $this->getHtmlPurifier($blocks['transfer']['text']);
		$this->template->clima = $this->getHtmlPurifier($blocks['clima']['text']);
    }
}