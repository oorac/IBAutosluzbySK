<?php

declare(strict_types=1);

namespace App\Module\Front\Homepage;

use App\Module\Front\BasePresenter;
use Nette\Application\UI\Form;
use Nette\Neon\Exception;
use Tracy\Debugger;

final class HomepagePresenter extends BasePresenter
{
    public function actionDefault()
    {
        $blocks = $this->block->getContentByName('homepage');
		$this->template->referencies = $this->reference->getAllReferenceVisible();
		$this->template->count = $this->reference->getAllReferenceVisibleCount();
        $this->template->aboutFirstPart = mb_strimwidth($this->getHtmlPurifier($blocks['about']['text']), 0, 500,"...");
        $this->template->aboutLastPart =  mb_strimwidth($this->getHtmlPurifier($blocks['about']['text']), 497, 100, "......");
    }

	protected function createComponentSearchCars(): Form
	{
		$form = new Form;
		try{
			$type = $form->addSelect('type_id', '', $this->car->getAllTypesPairs())
				->setPrompt('Vyberte typ vozidla');

			// Závislý select mark na type
			$mark = $form->addSelect('mark_id', '')
				->setPrompt('Vyberte značku vozidla');

			$type->setHtmlAttribute('data-url', $this->link(':Front:Endpoint:mark', '#'))
				->setHtmlAttribute('data-dependent', $mark->getHtmlName());

			$form->onAnchor[] = fn() =>
			$mark->setItems($type->getValue() ? $this->car->getAllMarksByTypeIdPairs($type->getValue()) : []);

//		$form->onAnchor[] = function () use ($mark, $type){
//			$mark->setItems($type->getValue() ? $this->car->getAllMarksByTypeIdPairs($type->getValue()) : []);
//		};

			// Další závislý select model na mark
			$model = $form->addSelect('model_id', '')
				->setPrompt('Vyberte model vozidla');

			$mark->setHtmlAttribute('data-url', $this->link(':Front:Endpoint:model', '#'))
				->setHtmlAttribute('data-dependent', $model->getHtmlName());

			$form->onAnchor[] = fn() =>
			$model->setItems($type->getValue() ? $this->car->getAllModelByMarkIdPairs($mark->getValue()) : []);

			$form->addSelect('fuel_id', '', $this->car->getAllFuelsPairs())
				->setPrompt('Vyberte typ paliva');

			$actualYear = date('Y');
			$yearArray = [];
			for($i = 2009; $i < $actualYear + 1; $i++)
			{
				$yearArray[$i] = $i;
			}
			$year = array_reverse($yearArray, true);

			$priceArray = [
				'10000' => '10000',
				'15000' => '15000',
				'20000' => '20000',
				'25000' => '25000',
				'30000' => '30000',
				'35000' => '35000',
			];
			$price = array_reverse($priceArray, true);

			$form->addSelect('year_from', 'Rok výroby od:', $yearArray)
				->setPrompt('Rok výroby od');
			$form->addSelect('year_to', 'Rok výroby do:', $year)
				->setPrompt('Rok výroby do');
			$form->addSelect('price_from', 'Cena od:', $priceArray)
				->setPrompt('Cena vozidla od');
			$form->addSelect('price_to', 'Cena do:', $price)
				->setPrompt('Cena vozidla do');

			$form->addSubmit('send', 'Vyhľadať');
			$form->onSuccess[] = [$this, 'processForm'];
		}
		catch(Exception $e)
		{
			Debugger::log($e, Debugger::ERROR);
		}
		return $form;
	}

	public function processForm(Form $form, array $values): void
	{
		$this->restoreRequest($this->backlink);
		$this->redirect(':Front:Car:', [$values]);
	}
}
