<?php

namespace App\Module\Front;

use App\Forms\SignInFormFactory;
use App\Forms\SignUpFormFactory;
use App\Model\Car;
use App\Model\Block;
use App\Model\Newsletter;
use App\Model\Reference;
use App\Model\Settings;
use App\Services\CarManager;
use App\Services\Root;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use HTMLPurifier;
use HTMLPurifier_Config;
use App\Services\Mailer;
use Nette\Neon\Exception;
use Tracy\Debugger;

abstract class BasePresenter extends Presenter
{
    const FM_SUCCESS = 'success';
    const FM_INFO = 'info';
    const FM_WARNING = 'warning';
    const FM_ERROR = 'danger';

	const SUBJECT_EMAIL = [
		'vseobecny' => 'Všeobecný dotaz',
		'ponuka' => 'Ponuka vozidla',
		'servis' => 'Servis',
		'doplnkova' => 'Doplnková služba',
	];

    protected Car $car;
    protected Block $block;
    protected SignInFormFactory $signInFactory;
    protected SignUpFormFactory $signUpFactory;
	protected Settings $settings;
	protected Reference $reference;
	protected Mailer $mailer;
	protected Newsletter $newsletter;
	protected Debugger $debugger;
	protected string $backlink = '';

	protected string $errorMessage = 'Nastala chyba, zkuste to prosím znovu později. Omlouváme se za potíže.';
	protected Root $root;
	protected CarManager $carManager;

	public function __construct(
		Root $root,
        Car               $car,
        Block             $block,
		Settings          $settings,
        SignInFormFactory $signInFactory,
        SignUpFormFactory $signUpFactory,
		Reference		  $reference,
		Mailer			  $mailer,
		Newsletter		  $newsletter,
		CarManager $carManager,
    )
    {
        parent::__construct();
        $this->car = $car;
        $this->block = $block;
        $this->signInFactory = $signInFactory;
        $this->signUpFactory = $signUpFactory;
		$this->settings = $settings;
		$this->reference = $reference;
		$this->mailer = $mailer;
		$this->newsletter = $newsletter;
		$this->root = $root;
		$this->carManager = $carManager;
	}

    protected function beforeRender()
    {
        parent::beforeRender();
		$this->backlink = $this->storeRequest();
        $this->template->blokyBase = $this->block->getContentByName('base');
        $this->template->basePath = '/';
        $this->template->web = $this->settings->getPageInfoAllPairs();
    }

	protected function createComponentSubscriberNewsletter(): Form
	{
		$form = new Form;
		$form->addText('email', '')
			->setHtmlAttribute('placeholder', 'Vložte svoj e-mail')
			->addRule(Form::FILLED, 'Zadajte e-mail');

		$form->addSubmit('send', 'Odoslať');
		$form->onSuccess[] = [$this, 'processFormNewsletter'];
		return $form;
	}

	public function processFormNewsletter(Form $form, array $values): void
	{
		$values['hash_url'] = md5(date('d.m.Y'));
		$this->newsletter->addNewsletter($values);
		$web = $this->settings->getPageInfoAllPairs();
		$htmlBody = array(
			'latte' => 'newsletter.latte',
			'latteAdmin' => 'newsletterAdmin.latte',
			'params' => [
				'email' => $values['email'],
			],
			'imagePath' => $this->root->getBasePath() . '/../www/',
			'templatesPath' => $this->root->getBasePath()
		);

		$response = $this->mailer->sendMail($values['email'], $web['email'], '', 'Potvrdenie odberu z webu IBAutoslužby.sk', $htmlBody);
		if($response == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Homepage:');
		}
		sleep('2');
		$response2 = $this->mailer->sendMail($web['email'], $web['email'], '', 'Nový odber na webu IBAutoslužby.sk', $htmlBody);
		if($response2 == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Homepage:');
		}
		$this->flashMessage('Odber noviniek bol zaevidovaný.', self::FM_SUCCESS);
		$this->restoreRequest($this->backlink);
		$this->redirect(':Front:Homepage:');
	}

	protected function createComponentContactForm(): Form
	{
		$form = new Form;
		$form->addText('name', '')
			->setHtmlAttribute('placeholder', 'Meno')
			->addRule(Form::FILLED, 'Zadajte meno');
		$form->addText('email', '')
			->setHtmlAttribute('placeholder', 'Email')
			->addRule(Form::FILLED, 'Zadajte email')
			->addRule(Form::EMAIL, 'Email nemá správny formát');
		$form->addText('phone', '')
			->setHtmlAttribute('placeholder', 'Telefón')
			->addRule(Form::FILLED, 'Zadajte telefón');
		$form->addSelect('subject', 'Predmet', self::SUBJECT_EMAIL);
		$form->addTextArea('message', '')
			->setHtmlAttribute('placeholder', 'Správa')
			->addRule(Form::FILLED, 'Zadajte správu');

		$form->addSubmit('send', 'Odoslať');
		$form->onSuccess[] = [$this, 'processFormContact'];
		return $form;
	}

	public function processFormContact(Form $form, array $values): void
	{
		$web = $this->settings->getPageInfoAllPairs();
		$htmlBody = array(
			'params' => [
				'name' => $values['name'],
				'email' => $values['email'],
				'phone' => $values['phone'],
				'message' => $values['message'],
				'subject' => self::SUBJECT_EMAIL[$values['subject']],
			],
			'imagePath' => $this->root->getBasePath() . '/../www/',
			'templatesPath' => $this->root->getBasePath()
		);
		$htmlBody['latte'] = 'contact.latte';
		$response = $this->mailer->sendMail($values['email'], $web['email'], '', 'kopie |' . self::SUBJECT_EMAIL[$values['subject']] . ' | IBAutoslužby.sk', $htmlBody);
		if($response == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Homepage:');
		}
		$htmlBody['latte'] = 'contactAdmin.latte';
		$response2 = $this->mailer->sendMail($web['email'], $web['email'], '', self::SUBJECT_EMAIL[$values['subject']] . ' | IBAutoslužby.sk', $htmlBody);
		if($response2 == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Homepage:');
		}

		$this->flashMessage('Správa bola odoslaná', self::FM_SUCCESS);
		$this->restoreRequest($this->backlink);
		$this->redirect(':Front:Homepage:');
	}

    protected function getHtmlPurifier($data){
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8'); // replace with your encoding
        $config->set('HTML.Doctype', 'HTML 4.01 Transitional'); // replace with your doctype
        $def = $config->getHTMLDefinition(true);
        $def->addAttribute('h4', 'target', 'Enum#_blank,_self,_target,_top');
        $purifier = new HTMLPurifier($config);
        return $purifier->purify($data);
    }
}
