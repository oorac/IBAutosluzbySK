<?php declare(strict_types=1);

namespace App\Module\Front\Reference;

use App\Module\Front\BasePresenter;
use Nette\Application\UI\Form;

final class ReferencePresenter extends BasePresenter
{
    public function actionDefault()
    {
        $blocks = $this->block->getContentByName('reference');
        $clean_html = $this->getHtmlPurifier($blocks['reference']['text']);
        $this->template->blocks = $clean_html;
    }

	protected function createComponentAddReferenceForm(): Form
	{
		$form = new Form;
		$form->addText('name', 'Meno');
		$form->addTextArea('text', 'Obsah referencie')
			->addRule($form::LENGTH, 'Zadajte minimálne %1$d a maximálne %2$d znakov.', [5, 255]);
		$form->onSuccess[] = [$this, 'processForm'];
		$form->addSubmit('submit', 'Uložit');
		return $form;
	}

	public function processForm(Form $form, $values): void
	{
		$values['hidden'] = 0;
		$this->reference->addReference($values);
		$this->flashMessage('Ďakujeme za Vašu spätnú väzbu', self::FM_INFO);
		$this->redirect(':Front:Homepage:');
	}
}