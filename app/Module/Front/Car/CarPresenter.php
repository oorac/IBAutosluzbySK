<?php declare(strict_types=1);

namespace App\Module\Front\Car;

use App\Module\Front\BasePresenter;
use Nette\Application\UI\Form;
use Nette\Http\IResponse;
use Nette\Utils\Paginator;

final class CarPresenter extends BasePresenter
{
	public const SPEEDOMETER = array(
		'100000' => 'do 100 000 km',
		'150000' => 'do 150 000 km',
		'200000' => 'do 200 000 km',
		'-1' => 'viac ako 200 000 km'
	);

	private $filter;

	private function setDefaultsForFilterForm($values)
	{
		$filterCars = $this['filterCars'];
		assert($filterCars instanceof Form);
		if(isset($values['parameter_id']) && !empty($values['parameter_id']))
			$values['parameter_data'] = json_encode($values['parameter_id'], JSON_PRETTY_PRINT);

		if(isset($values['mark_id']) && !empty($values['mark_id']))
			$filterCars['mark_id']->setItems($this->car->getAllMarksByTypeIdPairs($values['type_id']));
		if(isset($values['mark_id']) && !empty($values['mark_id']))
			$filterCars['model_id']->setItems($this->car->getAllModelByMarkIdPairs($values['mark_id']));
		if(isset($values['speedometer']) && !empty($values['speedometer']))
			$filterCars['speedometer']->setDefaultValue($values['speedometer']);
		if(isset($values['bodywork_id']) && !empty($values['bodywork_id']))
			$filterCars['bodywork_id']->setDefaultValue($values['bodywork_id']);
		if(isset($values['transmission_id']) && !empty($values['transmission_id']))
			$filterCars['transmission_id']->setDefaultValue($values['transmission_id']);
		if(isset($values['fuel_id']) && !empty($values['fuel_id']))
			$filterCars['fuel_id']->setDefaultValue($values['fuel_id']);
		$filterCars->setDefaults($values);
	}

    public function actionDefault(array $filter = [])
    {
		if(empty($this->getParameter('page')))
			$page = 1;
		else
			$page = $this->getParameter('page');

		if(!empty($filter))
		{
			$this->setDefaultsForFilterForm($filter);
		}

		if($this->filter)
			$carsCount = $this->car->getAllInfoAboutCarsCount($this->filter);
		else
			$carsCount = $this->car->getAllInfoAboutCarsCount($this->getParameters());
		$paginator = new Paginator;
		$paginator->setItemCount($carsCount->count);
		$paginator->setItemsPerPage(10);
		$paginator->setPage($page);

		if($this->filter)
        	$carsWithoutImages = $this->car->getAllInfoAboutCars($paginator->getLength(), $paginator->getOffset(), $this->filter);
		else
			$carsWithoutImages = $this->car->getAllInfoAboutCars($paginator->getLength(), $paginator->getOffset(), $this->getParameters());
		$carsWithImages = [];
        if (!empty($carsWithoutImages))
            foreach ($carsWithoutImages as $car)
            {
                $car['main_images'] = $this->car->getMainImagesByCarId($car['id'])->fetch();
                $carsWithImages[] = $car;
            }
		$this->template->pathImages = $this->car::DIR_IMAGE;
        $this->template->paginator = $paginator;
        $this->template->cars = $carsWithImages;
        $this->template->parameter = $this->carManager->getParameterCount();
        $this->template->group_parameter = $this->carManager->getAllParametersByGroupParameters();
        $this->template->transmission = $this->carManager->getTransmissionCount();
        $this->template->bodywork = $this->carManager->getBodyworkCount();
        $this->template->fuel = $this->carManager->getFuelCount();
        $this->template->speedometer = $this->carManager->getCarsCountBySpeedometer(self::SPEEDOMETER);
    }

	public function handleCars(int $page = 1)
	{
		$carsCount = $this->car->getAllInfoAboutCarsCount($this->getParameters());

		$paginator = new Paginator;
		$paginator->setItemCount($carsCount->count);
		$paginator->setItemsPerPage(2);
		$paginator->setPage($page);

		$this->sendJson($this->car->getAllInfoAboutCars($paginator->getLength(), $paginator->getOffset(), $this->getParameters()));
	}

    public function actionDetail($carId)
    {
		$car = $this->car->getAllInfoAboutCarById($carId);
		if(!$car)
			$this->error('Auto už nie je k dispozícii', IResponse::S404_NOT_FOUND);

		$form = $this->getComponent('sendEmail');
		$form->setDefaults(['id' => $carId]);

        $parameters = $this->car->getAllParametersByCarId($carId)->fetchAssoc('id');
        $this->template->car = $car;
		$otherImages = $this->car->getOtherImagesByCarId($carId);
		$mainImage = $this->car->getMainImagesByCarId($carId)->fetchAssoc('id');
		$images = array_merge($mainImage, $otherImages);
		$this->template->pathImages = $this->car::DIR_IMAGE;
        $this->template->images = $images ? $images : [];
        $this->template->parameters = $parameters;
		$this->template->description = $this->getHtmlPurifier($car->description);
    }

	protected function createComponentSendEmail(): Form
	{
		$form = new Form;
		$form->addHidden('id');
		$form->addText('email', 'Vložte e-mail')
			->setRequired()
			->setHtmlAttribute('placeholder', 'Váš e-mail');
		$form->addText('phone', 'telefón')
			->setHtmlAttribute('placeholder', 'Váš telefón');
		$form->addButton('send', 'Poptat');
		$form->onSuccess[] = [$this, 'processForm'];
		return $form;
	}

	public function processForm(Form $form, $values): void
	{
		$car = $this->car->getAllInfoAboutCarById($values['id']);
		$web = $this->settings->getPageInfoAllPairs();
		$name = $car->mark . ' ' . $car->model;
		$htmlBody = array(
			'params' => [
				'email' => $values['email'],
				'phone' => $values['phone'],
				'car' => $car,
				'url' => $this->link(':Front:Car:detail', $car->id),
				'nameCar' => $name
			],
			'imagePath' => $this->root->getBasePath() . '/../www/',
			'templatesPath' => $this->root->getBasePath()
		);
		$htmlBody['latte'] = 'offerCar.latte';
		$response = $this->mailer->sendMail($values['email'], $web['email'], '', 'kopie | Ponuka ' . $name . ' | IBAutoslužby.sk', $htmlBody);
		if($response == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Car:');
		}
		$htmlBody['latte'] = 'offerCarAdmin.latte';
		$response2 = $this->mailer->sendMail($web['email'], $web['email'], '', 'Nová ponuka ' . $name . ' | IBAutoslužby.sk', $htmlBody);
		if($response2 == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Car:');
		}

		$this->flashMessage('Správa bola odoslaná', self::FM_SUCCESS);
		$this->restoreRequest($this->backlink);
		$this->redirect(':Front:Car:');
	}

	protected function createComponentFilterCars(): Form
	{
		$form = new Form;
		$type = $form->addSelect('type_id', 'Typ', $this->car->getAllTypesPairs())
			->setPrompt('Vyber zo zoznamu');

		// Závislý select mark na type
		$mark = $form->addSelect('mark_id', 'Značka')
			->setPrompt('Vyber zo zoznamu');

		$type->setHtmlAttribute('data-url', $this->link(':Front:Endpoint:mark', '#'))
			->setHtmlAttribute('data-dependent', $mark->getHtmlName());

		$form->onAnchor[] = fn() =>
		$mark->setItems($type->getValue() ? $this->car->getAllMarksByTypeIdPairs($type->getValue()) : []);

		// Další závislý select model na mark
		$model = $form->addSelect('model_id', 'Model')
			->setPrompt('Vyber zo zoznamu');

		$mark->setHtmlAttribute('data-url', $this->link(':Front:Endpoint:model', '#'))
			->setHtmlAttribute('data-dependent', $model->getHtmlName());

		$form->onAnchor[] = fn() =>
		$model->setItems($type->getValue() ? $this->car->getAllModelByMarkIdPairs($mark->getValue()) : []);

		$form->addRadioList('bodywork_id', 'Karoséria', $this->car->getAllBodyworkPairs());
		$form->addMultiSelect('parameter_id', 'Výbava', $this->car->getAllParametersPairs());
		$form->addHidden('parameter_data');
		$form->addRadioList('engine_id', 'Motor vozidla', $this->car->getAllEnginePairs());
		$form->addRadioList('fuel_id', 'Palivo', $this->car->getAllFuelsPairs());
		$form->addRadioList('transmission_id', 'Prevodovka', $this->car->getAllTransmissionPairs());
		$form->addText('performance_from', 'Výkon');
		$form->addText('performance_to', 'Výkon');
		$form->addText('volume_from', 'Objem valcov');
		$form->addText('volume_to', 'Objem valcov');

		$form->addRadioList('speedometer', 'Najeto', self::SPEEDOMETER);

		$form->addText('year_from', 'Rok výroby do');
		$form->addText('year_to', 'Rok výroby do');

		$form->addText('price_from', 'Cena od');
		$form->addText('price_to', 'Cena do');

		$form->addSubmit('send', 'Filtruj');
		$form->addSubmit('storno', 'Zruš filtr')->onClick[] = [$this, 'stornoFormFilter'];
		$form->onSuccess[] = [$this, 'processFormFilter'];

		return $form;
	}

	public function stornoFormFilter(Form $form, $values)
	{
		$this->redirect(':Front:Car:');
	}

	public function processFormFilter(Form $form, array $values)
	{
		$newValues = [];
		foreach ($values as $value => $key) {
			if(!empty($key) && !is_null($key) && !is_array($key))
				$newValues[$value] = $key;
			elseif(is_array($key) && !empty($key))
				$newValues[$value] = $key;
		}

		$this->redirect(':Front:Car:', [$newValues]);
	}

	protected function createComponentNothingCarsForm(): Form
	{
		$form = new Form;
		$form->addText('name', '')
			->setHtmlAttribute('placeholder', 'Meno')
			->addRule(Form::FILLED, 'Zadajte meno');
		$form->addText('email', '')
			->setHtmlAttribute('placeholder', 'Email')
			->addRule(Form::FILLED, 'Zadajte email')
			->addRule(Form::EMAIL, 'Email nemá správny formát');
		$form->addText('phone', '')
			->setHtmlAttribute('placeholder', 'Telefón')
			->addRule(Form::FILLED, 'Zadajte telefón');
		$form->addText('subject', 'Predmet')
			->setDefaultValue('Ponuka vozidla');
		$form->addTextArea('message', '')
			->setHtmlAttribute('placeholder', 'Správa')
			->addRule(Form::FILLED, 'Zadajte správu')
			->setDefaultValue($this->getMessageNothingCars());

		$form->addSubmit('send', 'Odoslať');
		$form->onSuccess[] = [$this, 'processFormNothingCars'];
		return $form;
	}

	private function getMessageNothingCars(): string
	{
		$data = $this->getParameter('filter');
		$message = '';
		if(!empty($data['type_id']))
		{
			$type = $this->car->getTypeById($data['type_id']);
			$message .= 'Typ: ' . $type->name . "\r\n";
		}
		if(!empty($data['mark_id']))
		{
			$mark = $this->car->getMarkById($data['mark_id']);
			$message .= 'Značka: ' . $mark->name . "\r\n";
		}
		if(!empty($data['model_id']))
		{
			$model = $this->car->getModelById($data['model_id']);
			$message .= 'Model: ' . $model->name . "\r\n";
		}
		if(!empty($data['bodywork_id']))
		{
			if(is_array($data['bodywork_id']))
			{
				$message .= 'Karosérie: ';
				foreach ($data['bodywork_id'] as $bodywork)
				{
					$body = $this->car->getBodyworkById($bodywork);
					$message .= $body->name . "\r\n";
				}
			}
			else {
				$body = $this->car->getBodyworkById($data['bodywork_id']);
				$message .= 'Karoséria: ' . $body->name . "\r\n";
			}
		}
		if(!empty($data['fuel_id']))
		{
			if(is_array($data['fuel_id']))
			{
				$message .= 'Palivá: ';
				foreach ($data['fuel_id'] as $fuel)
				{
					$fuelItem = $this->car->getFuelById($fuel);
					$message .= $fuelItem->name . "\r\n";
				}
			}
			else{
				$fuelItem = $this->car->getFuelById($data['fuel_id']);
				$message .= 'Palivo: ' . $fuelItem->name . "\r\n";
			}
		}
		if(!empty($data['engine_id']))
		{
			if(is_array($data['engine_id']))
			{
				$message .= 'Motory: ';
				foreach ($data['engine_id'] as $engine)
				{
					$engineItem = $this->car->getEngineById($engine);
					$message .= $engineItem->name . "\r\n";
				}
			}
			else{
				$engineItem = $this->car->getEngineById($data['engine_id']);
				$message .= 'Motor: ' . $engineItem->name . "\r\n";
			}
		}
		if(!empty($data['transmission_id']))
		{
			if(is_array($data['transmission_id']))
			{
				$message .= 'Prevodovky: ';
				foreach ($data['engine_id'] as $transmission)
				{
					$transmissionItem = $this->car->gettransmissionById($transmission);
					$message .= $transmissionItem->name . "\r\n";
				}
			}
			else{
				$transmissionItem = $this->car->gettransmissionById($data['transmission_id']);
				$message .= 'Prevodovka: ' . $transmissionItem->name . "\r\n";
			}
		}
		if(!empty($data['parameter_id']))
		{
			if(is_array($data['parameter_id']))
			{
				$message .= 'Výbava: ';
				foreach ($data['parameter_id'] as $parameter)
				{
					$parameterItem = $this->car->getParameterById($parameter);
					$message .= $parameterItem->name . "\r\n";
				}
			}
			else{
				$parameterItem = $this->car->getParameterById($data['parameter_id']);
				$message .= 'Výbava: ' . $parameterItem->name . "\r\n";
			}
		}
		if(!empty($data['performance_from']))
		{
			$message .= 'Výkon od ' . $data['performance_from'] . "\r\n";
		}
		if(!empty($data['performance_to']))
		{
			$message .= 'Výkon do ' . $data['performance_to'] . "\r\n";
		}
		if(!empty($data['volume_from']))
		{
			$message .= 'Objem od ' . $data['volume_from'] . "\r\n";
		}
		if(!empty($data['volume_to']))
		{
			$message .= 'Objem do ' . $data['volume_to'] . "\r\n";
		}
		if(!empty($data['year_from']))
		{
			$message .= 'Rok výroby od ' . $data['year_from'] . "\r\n";
		}
		if(!empty($data['year_to']))
		{
			$message .= 'Rok výroby do ' . $data['year_to'] . "\r\n";
		}
		if(!empty($data['price_from']))
		{
			$message .= 'Cena od ' . $data['price_from'] . ' €' . "\r\n";
		}
		if(!empty($data['price_to']))
		{
			$message .= 'Cena do ' . $data['price_to'] . ' €' . "\r\n";
		}
		if(!empty($data['speedometer']))
		{
			$message .= self::SPEEDOMETER[$data['speedometer']];
		}
		return $message;
	}

	public function processFormNothingCars(Form $form, array $values)
	{
		$web = $this->settings->getPageInfoAllPairs();
		$htmlBody = array(
			'params' => [
				'name' => $values['name'],
				'email' => $values['email'],
				'phone' => $values['phone'],
				'message' => $values['message'],
				'subject' => $values['subject'],
			],
			'imagePath' => $this->root->getBasePath() . '/../www/',
			'templatesPath' => $this->root->getBasePath()
		);
		$htmlBody['latte'] = 'nothingCars.latte';
		$response = $this->mailer->sendMail($values['email'], $web['email'], '', 'kopie |' . $values['subject'] . ' | IBAutoslužby.sk', $htmlBody);
		if($response == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Car:');
		}
		$htmlBody['latte'] = 'nothingCarsAdmin.latte';
		$response2 = $this->mailer->sendMail($web['email'], $web['email'], '', $values['subject'] . ' | IBAutoslužby.sk', $htmlBody);
		if($response2 == false)
		{
			$this->flashMessage($this->errorMessage, self::FM_ERROR);
			$this->restoreRequest($this->backlink);
			$this->redirect(':Front:Car:');
		}

		$this->flashMessage('Správa bola odoslaná', self::FM_SUCCESS);
		$this->restoreRequest($this->backlink);
		$this->redirect(':Front:Car:');
	}
}