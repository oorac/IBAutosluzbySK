<?php declare(strict_types=1);

namespace App\Module\Front\Newsletter;

use App\Module\Front\BasePresenter;

final class NewsletterPresenter extends BasePresenter
{
    public function actionOut()
    {
		$hash_url = $this->getParameters();
		if(isset(array_keys($hash_url)[1]) && !empty(array_keys($hash_url)[1]))
		{
			$hash = array_keys($hash_url)[1];
			$email = $this->settings->getPageInfoSpecific('email');
			$newsletter = $this->newsletter->getNewsletterByHash($hash);
			if(!$newsletter)
				$this->redirect(':Front:Homepage:');
			$this->newsletter->deleteNewsletter($newsletter->id);
			$htmlBody = array(
				'latte' => 'newsletter',
				'params' => [$newsletter->email],
				'path' => $this->mailer->getBasePath()
			);
			bdump($htmlBody);
			exit;
			$this->mailer->sendMail($newsletter['email'], $email->value, '', 'Odhlásenie odberu noviniek.', $htmlBody);
			$this->mailer->sendMail($email->value, $email->value, '', 'Odhlásenie odberu noviniek.', $htmlBody);
			$this->flashMessage('Odber noviniek bol úspešne odhlásený.', self::FM_SUCCESS);
		}
		$this->redirect(':Front:Homepage:');
    }
}