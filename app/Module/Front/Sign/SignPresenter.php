<?php

declare(strict_types=1);

namespace App\Module\Front\Sign;

use App\Module\Front\BasePresenter;
use Nette;
use Nette\Application\UI\Form;


final class SignPresenter extends BasePresenter
{

    /**
     * Sign-in form factory.
     */
    protected function createComponentSignInForm(): Form
    {
        return $this->signInFactory->create(function (): void {
			if ($this->getUser()->isLoggedIn() && $this->getUser()->isInRole('ADMIN'))
			{
				$this->flashMessage('Vítejte v admin sekci.', self::FM_INFO);
				$this->redirect(':Admin:Homepage:default');
			}
        });
    }

    /**
     * Sign-up form factory.
     */
    protected function createComponentSignUpForm(): Form
    {
        return $this->signUpFactory->create(function (): void {
            $this->flashMessage('Byl jste registrován. Vyčkejte na schválení adminem.', self::FM_WARNING);
            $this->redirect(':Front:Sign:in');
        });
    }

    public function actionOut(): void
    {
        $this->getUser()->logout();
		$this->flashMessage('Bol ste úspešne odhlásený.', self::FM_INFO);
		$this->redirect(':Front:Sign:in');
    }
}
