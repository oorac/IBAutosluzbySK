<?php

namespace App\Module\Front\Endpoint;

use App\Module\Front\BasePresenter;

class EndpointPresenter extends BasePresenter
{
	public function actionMark($typeId): void
	{
		$items = $this->car->getAllMarksByTypeIdPairs($typeId);
		$this->sendJson($items);
	}

//	public function actionMark($id, $count = null, $offset = null): void
//	{
//		//$items = $this->car->getAllMarksByTypeIdPairs($id);
//		$test = $this->car->getAllMarksByTypeId($id, $count, $offset);
//		$items = array(
//			'items' => (array) $test
//		);
//		bdump($items);
//		$this->sendJson( $items);
//	}

	public function actionModel($id): void
	{
		$items = $this->car->getAllModelByMarkIdPairs($id);
		$this->sendJson($items);
	}

	public function actionBodywork($typeId)
	{
		$items = $this->car->getAllBodyworkByTypeIdPairs($typeId);
		$this->sendJson($items);
	}
}