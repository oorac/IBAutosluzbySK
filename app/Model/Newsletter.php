<?php

namespace App\Model;

class Newsletter extends Database
{
	public function addNewsletter($data)
    {
        return $this->getNewsletter()
            ->insert($data);
    }

    public function updateNewsletter($newsId, $data)
    {
        return $this->getNewsletter()
            ->where('id', $newsId)
            ->update($data);
    }

    public function deleteNewsletter($newsId)
    {
        return $this->getNewsletter()
            ->where('id', $newsId)
            ->delete();
    }

	public function getAllNewsletter()
	{
		return $this->getNewsletter()
			->select('*')
			->fetchAll();
	}

	public function getNewsletterById($newsId)
	{
		return $this->getNewsletter()->get($newsId);
	}

	public function getNewsletterByHash($hash)
	{
		return $this->getNewsletter()
			->select('*')
			->where('hash_url', $hash)
			->fetch();
	}

    public function getNewsletter()
	{
        return $this->database->table('newsletter');
    }
}