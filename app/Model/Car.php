<?php

namespace App\Model;

class Car extends Database
{
	public const TEMP_DIR_IMAGE = '/car_images/temp/';
	public const DIR_IMAGE = '/car_images/';

	public function getCountCarsByToVariable($name, $value)
	{
		return $this->getCar()
			->select('COUNT(id) AS count')
			->where('NOT hidden')
		->where($name, '<='.$value)
		->fetch()
			->toArray();
	}

	public function getAllDrivePairs()
	{
		return $this->getDrive()
			->select('id, name')
			->fetchPairs('id', 'name');
	}

	public function getAllDrive()
	{
		return $this->getDrive()
			->select('*')
			->fetchAssoc('id');
	}

	public function getAllBodyworkPairs()
	{
		return $this->getBodywork()
			->select('id, name')
			->fetchPairs('id', 'name');
	}

	public function getAllBodyworkByTypeIdPairs($typeId)
	{
		return $this->getBodywork()
			->select('id, name')
			->where('type_id', $typeId)
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getCountCarByBodyworkId($bodyworkId)
	{
		return $this->getCar()
			->select('COUNT(id) AS count')
			->where('NOT hidden')
			->where('bodywork_id', $bodyworkId)
			->fetch()
			->toArray();
	}

	public function getAllBodywork()
	{
		return $this->getBodywork()
			->select('*');
	}

	public function getAllFuelsPairs()
	{
		return $this->getFuel()
			->select('id, name')
			->fetchPairs('id', 'name');
	}

	public function getAllFuels()
	{
		return $this->getFuel()
			->select('*');
	}

	public function getCountCarByFuelId($fuelId)
	{
		return $this->getCar()
			->select('COUNT(id) AS count')
			->where('NOT hidden')
			->where('fuel_id', $fuelId)
			->fetch()
			->toArray();
	}

	public function getCountCarBySpeedometer($value)
	{
		if($value == 0)
			return $this->getCar()
				->select('COUNT(id) AS count')
				->where('NOT hidden')
				->where('speedometer >=', $value)
				->fetch()
				->toArray();
		else
			return $this->getCar()
				->select('COUNT(id) AS count')
				->where('NOT hidden')
				->where('speedometer <=', $value)
				->fetch()
				->toArray();
	}

	public function getAllVatPairs()
	{
		return $this->getVat()
			->select('id, name')
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getAllMarks()
	{
		return $this->getMark()
			->select('mark.*, type.name AS type')
			->fetchAssoc('id');
	}

	public function getAllMarksByTypeId($typeId)
	{
		$result = $this->getMark()
			->select('id, name')
			->where('type_id', $typeId)
			->where('NOT hidden')
			->fetchAssoc('id');
		return $result;
	}

	public function getAllMarksByTypeIdPairs($typeId)
	{
		return $this->getMark()
			->select('id, name')
			->where('type_id', $typeId)
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getAllModel()
	{
		return $this->getModel()
			->select('model.*, mark.name AS mark, type.name AS type')
			->fetchAssoc('id');
	}

	public function getAllModelByMarkIdPairs($markId)
	{
		return $this->getModel()
			->select('id, name')
			->where('mark_id', $markId)
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getAllTypesPairs()
	{
		return $this->getType()
			->select('id, name')
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getAllTypes()
	{
		return $this->getType()
			->select('*')
			->fetchAll();
	}

	public function getAllEngineByModelIdPairs($id)
	{
		return $this->getEngine()
			->select('id, name')
			->where('NOT hidden')
			->where('model_id', $id)
			->fetchPairs('id', 'name');
	}

	public function getAllEngine()
	{
		return $this->getEngine()
			->select('*')
			->fetchAll();
	}

	public function getAllEnginePairs()
	{
		return $this->getEngine()
			->select('*')
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getAllTransmissionPairs()
	{
		return $this->getTransmission()
			->select('*')
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getCountCarByTransmissionId($transmissionId)
	{
		return $this->getCar()
			->select('COUNT(id) AS count')
			->where('NOT hidden')
			->where('transmission_id', $transmissionId)
			->fetch()
			->toArray();
	}

	public function getAllTransmissions()
	{
		return $this->getTransmission()
			->select('*');
	}

	public function getAllInfoAboutCarsAll(int $limit = null)
	{
		$query = $this->getCar()
			->select('car.*, vat.name AS vat, bodywork.name AS bodywork, 
                    mark.name AS mark, model.name AS model, fuel.name AS fuel, transmission.name AS transmission,
                    drive.name AS drive, engine.name AS engine,
                     car.id AS id');
		if(!empty($limit))
			$query->order('id')
				->limit($limit, 0);
		return $query->fetchAssoc('id');
	}

    public function getAllInfoAboutCars(?int $limit = null, ?int $offset = null, $data = null, $hidden = false)
    {
        $query = $this->getCar()
			->select('car.*, vat.name AS vat, bodywork.name AS bodywork, 
                    mark.name AS mark, model.name AS model, fuel.name AS fuel, transmission.name AS transmission,
                    drive.name AS drive, engine.name AS engine,
                     car.id AS id');
		if(!empty($data['filter']['type_id']))
		{
			$query->where('car.type_id', $data['filter']['type_id']);
		}
		if(!empty($data['filter']['mark_id']))
		{
			$query->where('car.mark_id', $data['filter']['mark_id']);
		}
		if(!empty($data['filter']['model_id']))
		{
			$query->where('car.model_id', $data['filter']['model_id']);
		}
		if(!empty($data['filter']['bodywork_id']))
		{
			if(is_array($data['filter']['bodywork_id']))
			{
				$condition = '';
				$parameters = [];
				foreach ($data['filter']['bodywork_id'] as $fuel)
				{
					if ($condition !== '') {
						$condition .= ' OR ';
					}
					$condition .= 'car.bodywork_id = ?';
					$parameters[] = $fuel;
				}

				$query->where($condition, $parameters);
			}
			else
				$query->where('car.bodywork_id', $data['filter']['bodywork_id']);
		}
		if(!empty($data['filter']['fuel_id']))
		{
			if(is_array($data['filter']['fuel_id']))
			{
				$condition = '';
				$parameters = [];
				foreach ($data['filter']['fuel_id'] as $fuel)
				{
					if ($condition !== '') {
						$condition .= ' OR ';
					}
					$condition .= 'car.fuel_id = ?';
					$parameters[] = $fuel;
				}

				$query->where($condition, $parameters);
			}
			else
				$query->where('car.fuel_id', $data['filter']['fuel_id']);
		}
		if(!empty($data['filter']['engine_id']))
		{
			if(is_array($data['filter']['engine_id']))
			{
				$condition = '';
				$parameters = [];
				foreach ($data['filter']['engine_id'] as $fuel)
				{
					if ($condition !== '') {
						$condition .= ' OR ';
					}
					$condition .= 'car.engine_id = ?';
					$parameters[] = $fuel;
				}

				$query->where($condition, $parameters);
			}
			else
				$query->where('car.engine_id', $data['filter']['engine_id']);
		}
		if(!empty($data['filter']['transmission_id']))
		{
			if(is_array($data['filter']['transmission_id']))
			{
				$condition = '';
				$parameters = [];
				foreach ($data['filter']['transmission_id'] as $fuel)
				{
					if ($condition !== '') {
						$condition .= ' OR ';
					}
					$condition .= 'car.transmission_id = ?';
					$parameters[] = $fuel;
				}

				$query->where($condition, $parameters);
			}
			else
				$query->where('car.transmission_id', $data['filter']['transmission_id']);
		}
		if(!empty($data['filter']['parameter_id']))
		{
			if(is_array($data['filter']['parameter_id']))
			{
				foreach ($data['filter']['parameter_id'] as $parameter)
				{
					$query->where(':car_parameter(car_id).parameter_id', $parameter);
				}
			}
			else
				$query->where(':car_parameter(car_id).parameter_id', $data['filter']['parameter_id']);
		}
		if(!empty($data['filter']['performance_from']))
		{
			$query->where('car.performance >=', $data['filter']['performance_from']);
		}
		if(!empty($data['filter']['performance_to']))
		{
			$query->where('car.performance <=', $data['filter']['performance_to']);
		}
		if(!empty($data['filter']['volume_from']))
		{
			$query->where('car.volume >=', $data['filter']['volume_from']);
		}
		if(!empty($data['filter']['volume_to']))
		{
			$query->where('car.volume <=', $data['filter']['volume_to']);
		}
		if(!empty($data['filter']['year_from']))
		{
			$query->where('car.year >=', $data['filter']['year_from']);
		}
		if(!empty($data['filter']['year_to']))
		{
			$query->where('car.year <=', $data['filter']['year_to']);
		}
		if(!empty($data['filter']['price_from']))
		{
			$query->where('car.price >=', $data['filter']['price_from']);
		}
		if(!empty($data['filter']['price_to']))
		{
			$query->where('car.price <=', $data['filter']['price_to']);
		}
		if(!empty($data['filter']['speedometer']))
		{
			if($data['filter']['speedometer'] == 0)
				$query->where('car.speedometer >', 200000);
			else
				$query->where('car.speedometer <=', $data['filter']['speedometer']);
		}
		if(!$hidden)
		{
			$query->where('NOT car.hidden');
		}
		else
		{
			$query->where('car.hidden');
		}
		if($limit !== null && $offset !== null)
		{
			$query->order('id')
				->limit($limit, $offset);
		}

        return $query->fetchAssoc('id');
    }

	public function getAllInfoAboutCarsCount($data = null, $hidden = false)
	{
		$query = $this->getCar()
			->select('COUNT(*) AS count');
		if(!empty($data['filter']['type_id']))
		{
			$query->where('car.type_id', $data['filter']['type_id']);
		}
		if(!empty($data['filter']['mark_id']))
		{
			$query->where('car.mark_id', $data['filter']['mark_id']);
		}
		if(!empty($data['filter']['model_id']))
		{
			$query->where('car.model_id', $data['filter']['model_id']);
		}
		if(!empty($data['filter']['bodywork_id']))
		{
			if(is_array($data['filter']['bodywork_id']))
			{
				$condition = '';
				$parameters = [];
				foreach ($data['filter']['bodywork_id'] as $fuel)
				{
					if ($condition !== '') {
						$condition .= ' OR ';
					}
					$condition .= 'car.bodywork_id = ?';
					$parameters[] = $fuel;
				}

				$query->where($condition, $parameters);
			}
			else
				$query->where('car.bodywork_id', $data['filter']['bodywork_id']);
		}
		if(!empty($data['filter']['fuel_id']))
		{
			if(is_array($data['filter']['fuel_id']))
			{
				$condition_fuel = '';
				$parameters_fuel = [];
				foreach ($data['filter']['fuel_id'] as $fuel)
				{
					if ($condition_fuel !== '') {
						$condition_fuel .= ' OR ';
					}
					$condition_fuel .= 'car.fuel_id = ?';
					$parameters_fuel[] = $fuel;
				}

				$query->where($condition_fuel, $parameters_fuel);
			}
			else
				$query->where('car.fuel_id', $data['filter']['fuel_id']);
		}
		if(!empty($data['filter']['engine_id']))
		{
			if(is_array($data['filter']['engine_id']))
			{
				$condition_engine = '';
				$parameters_engine = [];
				foreach ($data['filter']['engine_id'] as $engine)
				{
					if ($condition_engine !== '') {
						$condition_engine .= ' OR ';
					}
					$condition_engine .= 'car.engine_id = ?';
					$parameters_engine[] = $engine;
				}

				$query->where($condition_engine, $parameters_engine);
			}
			else
				$query->where('car.engine_id', $data['filter']['engine_id']);
		}
		if(!empty($data['filter']['transmission_id']))
		{
			if(is_array($data['filter']['transmission_id']))
			{
				$condition_transmission = '';
				$parameters_transmission = [];
				foreach ($data['filter']['transmission_id'] as $fuel)
				{
					if ($condition_transmission !== '') {
						$condition_transmission .= ' OR ';
					}
					$condition_transmission .= 'car.transmission_id = ?';
					$parameters_transmission[] = $fuel;
				}

				$query->where($condition_transmission, $parameters_transmission);
			}
			else
				$query->where('car.transmission_id', $data['filter']['transmission_id']);
		}
		if(!empty($data['filter']['parameter_id']))
		{
			if(is_array($data['filter']['parameter_id']))
			{
//				$where = [];
//				foreach ($data['filter']['parameter_id'] as $parameter)
//				{
//					$where = array(
//						':car_parameter(car_id).parameter_id' =>  $parameter
//					);
//				}
				$where = '';
				foreach ($data['filter']['parameter_id'] as $parameter)
				{
					if($where === '')
						$where .= $parameter;
					else
						$where .= ', ' . $parameter;
				}

				$query->where(':car_parameter(car_id).parameter_id', [$where]);
			}
			else
				$query->where(':car_parameter(car_id).parameter_id', $data['filter']['parameter_id']);
		}
		if(!empty($data['filter']['performance_from']))
		{
			$query->where('car.performance >=', $data['filter']['performance_from']);
		}
		if(!empty($data['filter']['performance_to']))
		{
			$query->where('car.performance <=', $data['filter']['performance_to']);
		}
		if(!empty($data['filter']['volume_from']))
		{
			$query->where('car.volume >=', $data['filter']['volume_from']);
		}
		if(!empty($data['filter']['volume_to']))
		{
			$query->where('car.volume <=', $data['filter']['volume_to']);
		}
		if(!empty($data['filter']['year_from']))
		{
			$query->where('car.year >=', $data['filter']['year_from']);
		}
		if(!empty($data['filter']['year_to']))
		{
			$query->where('car.year <=', $data['filter']['year_to']);
		}
		if(!empty($data['filter']['price_from']))
		{
			$query->where('car.price >=', $data['filter']['price_from']);
		}
		if(!empty($data['filter']['price_to']))
		{
			$query->where('car.price <=', $data['filter']['price_to']);
		}
		if(!empty($data['filter']['speedometer']))
		{
			if($data['filter']['speedometer'] == -1)
				$query->where('car.speedometer >', 200000);
			else
				$query->where('car.speedometer <=', $data['filter']['speedometer']);
		}
		if(!$hidden)
		{
			$query->where('NOT car.hidden');
		}
		else
		{
			$query->where('car.hidden');
		}
		return $query->fetch();
	}

    public function getAllInfoAboutCarById($carId)
    {
        return $this->getCar()
            ->select('car.*, vat.name AS vat, bodywork.name AS bodywork, 
                    mark.name AS mark, model.name AS model, fuel.name AS fuel, transmission.name AS transmission,
                    drive.name AS drive, engine.name AS engine, type.name AS type,
                     car.id AS id')
            ->where('car.id', $carId)
            ->fetch();
    }

	public function getAllImagesByCarId($carId)
	{
		return $this->getImage()
			->select('image.*')
			->where(':car_image.car_id', $carId)
			->fetchAll();
	}

	public function getAllCarImagesByCarId($carId)
	{
		return $this->getCarImage()
			->select('*')
			->where('car_id', $carId)
			->fetchAll();
	}

	public function insertRelationImageCar($imageId, $carId)
	{
		return $this->getCarImage()
			->insert([
				'car_id' => $carId,
				'image_id' => $imageId,
			]);
	}

	public function getCarImage()
	{
		return $this->database->table('car_image');
	}

    public function getMainImagesByCarId($carId)
    {
        return $this->getImage()
            ->select('image.*')
            ->where(':car_image.car_id', $carId)
            ->where('image.main', 1);
    }

    public function getOtherImagesByCarId($carId)
    {
        return $this->getImage()
            ->select('*')
            ->where(':car_image.car_id', $carId)
            ->where('image.main', 0)
            ->fetchAssoc('id');
    }

    public function getAllParametersByCarId($carId)
    {
        return $this->getCarParameter()
            ->select('car_parameter.*, parameter.*, car_parameter.id AS id')
            ->where('car_parameter.car_id', $carId)
            ->where('parameter.hidden', 0);
    }

	public function getAllParametersByParameterGroupId($groupId)
	{
		return $this->getParameter()
			->select('parameter.*')
			->where('parameter_group(parameter_group_id).id', $groupId)
			->where('NOT parameter.hidden');
	}

	public function getAllParametersPairs()
	{
		return $this->getParameter()
			->select('id, name')
			->where('NOT hidden')
			->fetchPairs('id', 'name');
	}

	public function getCountCarByParameterId($parameterId)
	{
		return $this->getCarParameter()
			->select('COUNT(id) AS count')
			->where('parameter_id', $parameterId)
			->fetch()
			->toArray();
	}

	public function getAllParameters()
	{
		return $this->getParameter()
			->select('parameter.*, parameter_group.name AS group');
	}

	public function insertCarParameter($carId, $parameterId)
	{
		$exist = $this->getCarParameterByCarIdByCarParameterId($carId, $parameterId);
		if(!$exist)
			$this->getCarParameter()
				->insert([
					'car_id' => $carId,
					'parameter_id' => $parameterId,
				]);
	}

	public function getCarParameterByCarIdByCarParameterId($carId, $parameterId)
	{
		return $this->getCarParameter()
				->select('id')
			->where('car_id',$carId)
			->where('parameter_id',$parameterId)
			->fetch();
	}

    public function getCarParameter()
    {
        return $this->database->table('car_parameter');
    }

	public function updateParameter($data, $id)
	{
		return $this->getParameter()
			->where('id', $id)
			->update($data);
	}

	public function addParameter($data)
	{
		return $this->getParameter()
			->insert($data);
	}

	public function getParameters()
	{
		return $this->getParameter()
			->select('*')
			->fetchAll();
	}

	public function getParameterById($id)
	{
		return $this->getParameter()->get($id);
	}

	public function getAllGroupParametersPairs()
	{
		return $this->getGroupParameter()
			->select('id, name')
			->fetchPairs('id', 'name');
	}

	public function getAllGroupParameters()
	{
		return $this->getGroupParameter()
			->select('*');
	}

	public function getGroupParameter()
	{
		return $this->database->table('parameter_group');
	}

	public function getParameterId($parameterId)
	{
		return $this->getParameter()
			->select('*')
			->where('id', $parameterId)
			->fetch();
	}

	public function getParameter()
	{
		return $this->database->table('parameter');
	}

	public function deleteImage($imageId)
	{
		return $this->getImage()
			->where('id', $imageId)
			->delete();
	}

	public function deleteCarImage($carId, $imageId)
	{
		return $this->getCarImage()
			->where('image_id', $imageId)
			->where('car_id', $carId)
			->delete();
	}

	public function insertImage($image)
	{
		return $this->getImage()
			->insert($image);
	}

    public function getImage()
    {
        return $this->database->table('image');
    }

	public function deleteTransmission($transmissionId)
	{
		return $this->getTransmission()
			->where('id', $transmissionId)
			->delete();
	}

	public function updateTransmission($transmissionId, $data)
	{
		return $this->getTransmission()
			->where('id', $transmissionId)
			->update($data);
	}

	public function visibleTransmission($transmissionId)
	{
		return $this->getTransmission()
			->where('id', $transmissionId)
			->update( ['hidden' => '0']);
	}

	public function hiddenTransmission($transmissionId)
	{
		return $this->getTransmission()
			->where('id', $transmissionId)
			->update( ['hidden' => '1']);
	}

	public function getTransmissionById($id)
	{
		return $this->getMark()->get($id);
	}

	public function addTransmission($data)
	{
		return $this->getTransmission()
			->insert($data);
	}

	public function getTransmission()
	{
		return $this->database->table('transmission');
	}

	public function deleteMark($markId)
	{
		return $this->getMark()
			->where('id', $markId)
			->delete();
	}

	public function updateMark($markId, $data)
	{
		return $this->getMark()
			->where('id', $markId)
			->update($data);
	}

	public function visibleMark($markId)
	{
		return $this->getMark()
			->where('id', $markId)
			->update( ['hidden' => '0']);
	}

	public function hiddenMark($markId)
	{
		return $this->getMark()
			->where('id', $markId)
			->update( ['hidden' => '1']);
	}

	public function getMarkById($id)
	{
		return $this->getMark()->get($id);
	}

	private function getMarkByName($data)
	{
		return $this->getMark()
			->select('id')
			->where('name', $data['name'])
			->fetch();
	}

	public function addMark($data)
	{
		$exist = $this->getMarkByName($data);
		if(!$exist)
			return $this->getMark()
				->insert($data);
		else
			return $exist;
	}

	public function getMark()
	{
		return $this->database->table('mark');
	}

	public function deleteModel($modelId)
	{
		return $this->getModel()
			->where('id', $modelId)
			->delete();
	}

	public function updateModel($modelId, $data)
	{
		return $this->getModel()
			->where('id', $modelId)
			->update($data);
	}

	public function visibleModel($modelId)
	{
		return $this->getModel()
			->where('id', $modelId)
			->update( ['hidden' => '0']);
	}

	public function hiddenModel($modelId)
	{
		return $this->getModel()
			->where('id', $modelId)
			->update( ['hidden' => '1']);
	}

	public function getModelById($id)
	{
		return $this->getModel()->get($id);
	}

	private function getModelByName($data)
	{
		return $this->getModel()
			->select('id')
			->where('name', $data['name'])
			->fetch();
	}

	public function addModel($data)
	{
		$exist = $this->getModelByName($data);
		if(!$exist)
			return $this->getModel()
				->insert($data);
		else
			return $exist;
	}

	public function getModel()
	{
		return $this->database->table('model');
	}

	public function deleteType($typeId)
	{
		return $this->getType()
			->where('id', $typeId)
			->delete();
	}

	public function updateType($typeId, $data)
	{
		return $this->getType()
			->where('id', $typeId)
			->update($data);
	}

	public function visibleType($typeId)
	{
		return $this->getType()
			->where('id', $typeId)
			->update( ['hidden' => '0']);
	}

	public function hiddenType($typeId)
	{
		return $this->getType()
			->where('id', $typeId)
			->update( ['hidden' => '1']);
	}

	public function getTypeById($id)
	{
		return $this->getType()->get($id);
	}

	private function getTypeByName($data)
	{
		return $this->getType()
			->select('id')
			->where('name', $data['name'])
			->fetch();
	}

	public function addType($data)
	{
		$exist = $this->getTypeByName($data);
		if(!$exist)
			return $this->getType()
				->insert($data);
		else
			return $exist;
	}

	public function getType()
	{
		return $this->database->table('type');
	}

	public function deleteFuel($fuelId)
	{
		return $this->getFuel()
			->where('id', $fuelId)
			->delete();
	}

	public function updateFuel($fuelId, $data)
	{
		return $this->getFuel()
			->where('id', $fuelId)
			->update($data);
	}

	public function visibleFuel($fuelId)
	{
		return $this->getFuel()
			->where('id', $fuelId)
			->update( ['hidden' => '0']);
	}

	public function hiddenFuel($fuelId)
	{
		return $this->getFuel()
			->where('id', $fuelId)
			->update( ['hidden' => '1']);
	}

	public function getFuelById($id)
	{
		return $this->getFuel()->get($id);
	}

	public function addFuel($data)
	{
		return $this->getFuel()
			->insert($data);
	}

	public function getFuel()
	{
		return $this->database->table('fuel');
	}

	public function getVat()
	{
		return $this->database->table('vat');
	}

	public function deleteCar($carId)
	{
		return $this->getCar()
			->where('id', $carId)
			->delete();
	}

	public function updateCar($carId, $data)
	{
		return $this->getCar()
			->where('id', $carId)
			->update($data);
	}

	public function visibleCar($carId)
	{
		return $this->getCar()
			->where('id', $carId)
			->update( ['hidden' => '0']);
	}

	public function hiddenCar($carId)
	{
		return $this->getCar()
			->where('id', $carId)
			->update( ['hidden' => '1']);
	}

	public function getCarById($id)
	{
		return $this->getCar()->get($id);
	}

	public function addCar($data)
	{
		return $this->getCar()
			->insert($data);
	}

	public function getCar()
	{
		return $this->database->table('car');
	}

	public function deleteBodywork($bodyworkId)
	{
		return $this->getBodywork()
			->where('id', $bodyworkId)
			->delete();
	}

	public function updateBodywork($bodyworkId, $data)
	{
		return $this->getBodywork()
			->where('id', $bodyworkId)
			->update($data);
	}

	public function visibleBodywork($bodyworkId)
	{
		return $this->getBodywork()
			->where('id', $bodyworkId)
			->update( ['hidden' => '0']);
	}

	public function hiddenBodywork($bodyworkId)
	{
		return $this->getBodywork()
			->where('id', $bodyworkId)
			->update( ['hidden' => '1']);
	}

	public function getBodyworkById($id)
	{
		return $this->getBodywork()->get($id);
	}

	private function getBodyworkByName($data)
	{
		return $this->getBodywork()
			->select('id')
			->where('name', $data['name'])
			->fetch();
	}

	public function addBodywork($data)
	{
		$exist = $this->getBodyworkByName($data);
		if(!$exist)
			return $this->getBodywork()
				->insert($data);
		else
			return $exist;
	}

	public function getBodywork()
	{
		return $this->database->table('bodywork');
	}

	public function deleteEngine($engineId)
	{
		return $this->getEngine()
			->where('id', $engineId)
			->delete();
	}

	public function updateEngine($engineId, $data)
	{
		return $this->getEngine()
			->where('id', $engineId)
			->update($data);
	}

	public function visibleEngine($engineId)
	{
		return $this->getEngine()
			->where('id', $engineId)
			->update( ['hidden' => '0']);
	}

	public function hiddenEngine($engineId)
	{
		return $this->getEngine()
			->where('id', $engineId)
			->update( ['hidden' => '1']);
	}

	public function getEngineById($id)
	{
		return $this->getEngine()->get($id);
	}

	public function addEngine($data)
	{
		return $this->getEngine()
			->insert($data);
	}

	public function getEngine()
	{
		return $this->database->table('engine');
	}

	public function visibleParameter($parameterId)
	{
		return $this->getParameter()
			->where('id', $parameterId)
			->update( ['hidden' => '0']);
	}

	public function hiddenParameter($parameterId)
	{
		return $this->getParameter()
			->where('id', $parameterId)
			->update( ['hidden' => '1']);
	}

	public function getAllCarByParameterId($parameterId)
	{
		return $this->getCarParameter()
			->where('parameter_id', $parameterId)
			->fetchAll();
	}

	public function deleteParameter($parameterId)
	{
		return $this->getParameter()
			->where('id', $parameterId)
			->delete();

	}

	public function deleteDrive($driveId)
	{
		return $this->getDrive()
			->where('id', $driveId)
			->delete();
	}

	public function updateDrive($driveId, $data)
	{
		return $this->getDrive()
			->where('id', $driveId)
			->update($data);
	}

	public function visibleDrive($driveId)
	{
		return $this->getDrive()
			->where('id', $driveId)
			->update( ['hidden' => '0']);
	}

	public function hiddenDrive($driveId)
	{
		return $this->getDrive()
			->where('id', $driveId)
			->update( ['hidden' => '1']);
	}

	public function getDriveById($id)
	{
		return $this->getDrive()->get($id);
	}

	public function addDrive($data)
	{
		return $this->getDrive()
			->insert($data);
	}

	public function getDrive()
	{
		return $this->database->table('drive');
	}
}