<?php

namespace App\Model;

class Reference extends Database
{
	public function getAllReferenceVisible()
	{
		return $this->getReference()
			->select('*')
			->where('hidden = 0')
			->fetchAssoc('id');
	}

	public function getAllReferenceVisibleCount()
	{
		return $this->getReference()
			->select('COUNT(*) AS count')
			->where('hidden = 0')
			->fetch();
	}

    public function addReference($data)
    {
        return $this->getReference()
            ->insert($data);
    }

    public function updateReference($refId, $data)
    {
        return $this->getReference()
            ->where('id', $refId)
            ->update($data);
    }

    public function deleteReference($refId)
    {
        return $this->getReference()
            ->where('id', $refId)
            ->delete();
    }

	public function visibleReference($refId)
	{
		return $this->getReference()
			->where('id', $refId)
			->update( ['hidden' => '0']);
	}

	public function hiddenReference($refId)
	{
		return $this->getReference()
			->where('id', $refId)
			->update( ['hidden' => '1']);
	}

	public function getAllReference()
	{
		return $this->getReference()
			->select('*')
			->fetchAll();
	}

	public function getReferenceById($refId)
	{
		return $this->getReference()->get($refId);
	}

    public function getReference()
    {
        return $this->database->table('reference');
    }
}