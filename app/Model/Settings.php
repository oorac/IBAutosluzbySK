<?php

namespace App\Model;

class Settings extends Database
{
    public function addVat($data)
    {
        return $this->getVat()
            ->insert($data);
    }

    public function updateVat($vatId, $data)
    {
        return $this->getVat()
            ->where('id', $vatId)
            ->update($data);
    }

    public function deleteVat($vatId)
    {
        return $this->getVat()
            ->where('id', $vatId)
            ->delete();
    }

	public function visibleVat($vatId)
	{
		return $this->getVat()
			->where('id', $vatId)
			->update( ['hidden' => '0']);
	}

	public function hiddenVat($vatId)
	{
		return $this->getVat()
			->where('id', $vatId)
			->update( ['hidden' => '1']);
	}

	public function getAllVat()
	{
		return $this->getVat()
			->select('*')
			->fetchAll();
	}

	public function getVatById($vatId)
	{
		return $this->getVat()->get($vatId);
	}

	public function getPageInfoSpecific($name)
	{
		return $this->getPageInfo()
			->select('value')
			->where('name', $name)
			->fetch();
	}

	public function updatePageInfo($data, $pageInfoId)
	{
		return $this->getPageInfo()
			->where('id', $pageInfoId)
			->update($data);
	}

	public function getPageInfoAllPairs()
	{
		return $this->getPageInfo()
			->select('name, value')
			->fetchPairs('name', 'value');
	}

	public function getPageInfoAll()
	{
		return $this->getPageInfo()
			->select('*')
			->fetchAssoc('id');
	}

	public function getPageInfoById($id)
	{
		return $this->getPageInfo()
			->select('*')
			->where('id', $id)
			->fetch();
	}

	public function getPageInfo()
	{
		return $this->database->table('page_info');
	}

    public function getVat()
    {
        return $this->database->table('vat');
    }
}