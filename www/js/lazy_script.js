const observer = new IntersectionObserver(
    entries => {
        entries.foreach(entry => {
            entry.target.classList.toggle("show", entry.isIntersecting)
        })
    },
    {
        threshold: 1,
    }
)

const cardContainer = document.querySelector(".card-container")
function loadNewCards(){
    for (let i = 0; i < 10; i++){
        const card = document.createElement("div")
        card.textContent = "New Card"
        card.classList.add("card")
        observer.observe(card)
        cardContainer.append(card)
    }
}

const lastCardObserver = new IntersectionObserver(entries => {
    const lastCard = entries[0]
    if(!lastCard.isIntersecting) return
    loadNewCard()
    lastCardObserver.unobserve(lastCard.target)
    lastCardObserver.observer(document.querySelector(".card:last-child"))
}, {
    rootMargin: "100px"
})

lastCardObserver.observer(document.querySelector(".card:last-child"))

cards.forEach(card => {
    observer.observer(card)
})





// function fetchImage() {
//     if (imgCount !== 20) {
//         setTimeout(() => {
//             for (let i = imgCount; i < imgCount + 5; i++) {
//                 const image = document.createElement("img");
//                 image.src = data[i].src;
//                 rootElement.append(image);
//             }
//             imgCount += 5;
//         }, 500)
//     }
// }
// document.addEventListener("DOMContentLoaded", () => {
//     let options = {
//         root: null,
//         rootMargin: "0px",
//         threshold: 0.25
//     };
//
//     function handleIntersect(entries, observer) {
//         entries.forEach((entry) => {
//             if (entry.isIntersecting) {
//                 if (imgCount !== 20) {
//                     fetchImage()
//                 }
//             }
//         });
//     }
//
//     let observer = new IntersectionObserver(handleIntersect,
//         options);
//     observer.observe(loader);
// })